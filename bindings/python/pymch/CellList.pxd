from pymch cimport mch

cdef class CellList:
    cdef mch.CellList* thisptr

cdef class CellIterator:
    cdef mch.CellIterator* thisptr

cdef class CellNeighbourIterator:
    cdef mch.CellNeighbourIterator* thisptr

cdef class CellContentIterator:
    cdef mch.CellContentIterator* thisptr
