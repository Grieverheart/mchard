cimport mch
from cython.operator cimport dereference as deref
from cython.operator cimport preincrement as preinc
from pymch.clam cimport Vec3d

cdef class CellList:
    def __cinit__(self, CellList other = None):
        if other is not None:
            self.thisptr = new mch.CellList(deref(other.thisptr))
        else:
            self.thisptr = new mch.CellList()

    def __dealloc__(self):
        del self.thisptr

    def init(self, int nPart, Vec3d box_bounds, double minCellSize):
        return self.thisptr.init(nPart, box_bounds.v, minCellSize)

    def add(self, int pid, Vec3d pos):
        return self.thisptr.add(pid, pos.v)

    def update(self, int pid, Vec3d pos):
        return self.thisptr.update(pid, pos.v)

    def move(self, int pid, int coffset):
        return self.thisptr.move(pid, coffset)

    def rescale(self, double factor):
        return self.thisptr.rescale(factor)

    def should_rebuild(self, double factor):
        return self.thisptr.should_rebuild(factor)

    def cell_index(self, int pid):
        return self.thisptr.cell_index(pid)

    def cell_index_at_offset(self, int cid, int offset):
        return self.thisptr.cell_index_at_offset(cid, offset)

    def cell_size(self):
        ret = Vec3d()
        ret.v = self.thisptr.cell_size()
        return ret

    def cell_origin(self, int cid):
        ret = Vec3d()
        ret.v = self.thisptr.cell_origin(cid)
        return ret

    def cell_min_size(self):
        return self.thisptr.cell_min_size()

    def cells(self):
        ci = CellIterator()
        ci.thisptr = new mch.CellIterator(self.thisptr.cells())
        return ci

    def cell_vol_nbs(self, int pid):
        ci = CellNeighbourIterator()
        ci.thisptr = new mch.CellNeighbourIterator(self.thisptr.cell_vol_nbs(pid))
        return ci

    def cell_nbs(self, int cid):
        ci = CellNeighbourIterator()
        ci.thisptr = new mch.CellNeighbourIterator(self.thisptr.cell_nbs(cid))
        return ci

    def particle_cell_nbs(self, int pid):
        ci = CellNeighbourIterator()
        ci.thisptr = new mch.CellNeighbourIterator(self.thisptr.particle_cell_nbs(pid))
        return ci

    def particle_cell_nbs(self, Vec3d pos):
        ci = CellNeighbourIterator()
        ci.thisptr = new mch.CellNeighbourIterator(self.thisptr.particle_cell_nbs(pos.v))
        return ci

    def cell_dir_nbs(self, int cid, int direction):
        ci = CellNeighbourIterator()
        ci.thisptr = new mch.CellNeighbourIterator(self.thisptr.cell_dir_nbs(cid, direction))
        return ci

    def cell_content(self, int cid):
        ci = CellContentIterator()
        ci.thisptr = new mch.CellContentIterator(self.thisptr.cell_content(cid))
        return ci

    def push_front(self, CellNeighbourIterator itr):
        self.thisptr.push_front(deref(itr.thisptr))

    def cellIndex(self, Vec3d pos):
        return self.thisptr.cellIndex(pos.v)

cdef class CellIterator:
    def __cinit__(self):
        self.thisptr = NULL

    def __dealloc__(self):
        del self.thisptr

    def __iter__(self):
        return self

    def __next__(self):
        if deref(self.thisptr) != self.thisptr.end():
            idx = deref(deref(self.thisptr))
            preinc(deref(self.thisptr))
            return idx
        else:
            raise StopIteration()

cdef class CellNeighbourIterator:
    def __cinit__(self):
        self.thisptr = NULL

    def __dealloc__(self):
        del self.thisptr

    def __iter__(self):
        return self

    def __next__(self):
        if deref(self.thisptr) != self.thisptr.end():
            idx = deref(deref(self.thisptr))
            preinc(deref(self.thisptr))
            return idx
        else:
            raise StopIteration()

cdef class CellContentIterator:
    def __cinit__(self):
        self.thisptr = NULL

    def __dealloc__(self):
        del self.thisptr

    def __iter__(self):
        return self

    def __next__(self):
        if deref(self.thisptr) != self.thisptr.end():
            idx = deref(deref(self.thisptr))
            preinc(deref(self.thisptr))
            return idx
        else:
            raise StopIteration()
