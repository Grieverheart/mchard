cimport mch
from pymch.clam cimport Vec3d
from pymch cimport box
from pymch.shape.convex cimport *
from pymch.Particle cimport Particle
from cython.operator cimport dereference as deref
from libc.stdlib cimport malloc, free
from libc.string cimport memcpy

#TODO: Consider using numpy arrays

cdef class Configuration:

    def __cinit__(self):
        self.thisptr = NULL

    def __dealloc__(self):
        del self.thisptr

    def __copy__(self):
        ret = Configuration()
        ret.thisptr = new mch.Configuration(deref(self.thisptr))
        ret.shapes = self.shapes[:]
        memcpy(ret.thisptr, self.thisptr, sizeof(mch.Configuration))
        return ret

    #def __deepcopy__(self):
    #    ret = Configuration()
    #    ret.thisptr = new mch.Configuration(deref(self.thisptr))
    #    return ret

    @classmethod
    def create(cls, a_box, particles, shapes):
        ret = Configuration()

        #Copy shapes to keep pointers alive
        ret.shapes = shapes[:]

        #Handle box
        cdef mch.box.Base* c_box
        if type(a_box) is box.Rectangular:
            c_box = (<box.Rectangular>a_box).thisptr
        elif type(a_box) is box.Ellipsoidal:
            c_box = (<box.Ellipsoidal>a_box).thisptr
        elif type(a_box) is box.Base:
            c_box = (<box.Base>a_box).thisptr
        else:
            raise TypeError("Invalid argument 1, a box type is required, " + str(type(a_box)) + " given.")

        #Handle particles
        cdef mch.Particle* c_particles = <mch.Particle*>malloc(len(particles) * sizeof(mch.Particle))
        for i in range(len(particles)):
            c_particles[i] = deref((<Particle>particles[i]).thisptr)
            #TODO: Check if this works properly. Shapes have the same layout but not sure how it actually works behind the scenes.
            c_particles[i].shape = (<Polyhedron>ret.shapes[(<Particle>particles[i]).shape_id]).thisptr

        ret.thisptr = new mch.Configuration(c_box, len(particles), c_particles)

        free(c_particles)
        return ret

    @property
    def n_part(self):
        return self.thisptr.n_part_

    @property
    def particles(self):
        particles = []
        for i in range(self.thisptr.n_part_):
            particle = Particle()
            memcpy(particle.thisptr, &self.thisptr.particles_[i], sizeof(mch.Particle))
            particles.append(particle)
        return particles

    @property
    def box(self):
        c_box = mch.box.dynamic_cast_rectangular(self.thisptr.box_)
        if c_box is not NULL:
            p_box = box.Rectangular(Vec3d(1.0))
            memcpy((<box.Rectangular>p_box).thisptr, c_box, sizeof(mch.box.RectangularBox))
            return p_box

        c_box = mch.box.dynamic_cast_ellipsoidal(self.thisptr.box_)
        if c_box is not NULL:
            p_box = box.Ellipsoidal(Vec3d(1.0))
            memcpy((<box.Ellipsoidal>p_box).thisptr, c_box, sizeof(mch.box.EllipsoidalBox))
            return p_box

        raise TypeError("Unknown box type found.")

    @property
    def shapes(self):
        return self.shapes

