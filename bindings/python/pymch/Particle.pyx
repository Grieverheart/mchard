cimport mch
from pymch.clam cimport Vec3d, Quatd
from pymch.shape.convex cimport *
from cython.operator cimport dereference as deref
from libc.string cimport memcpy

cdef class Particle:
    def __cinit__(self):
        self.thisptr = new mch.Particle()

    def __dealloc__(self):
        del self.thisptr

    def __copy__(self):
        ret = Particle()
        memcpy(ret.thisptr, self.thisptr, sizeof(mch.Particle))
        ret.shape_id = self.shape_id
        return ret

    property pos:
        def __get__(self):
            pos = Vec3d()
            pos.v = self.thisptr.pos
            return pos

        def __set__(self, Vec3d new_pos):
            self.thisptr.pos = new_pos.v

    property rot:
        def __get__(self):
            rot = Quatd()
            rot.q = self.thisptr.rot
            return rot

        def __set__(self, Quatd new_rot):
            self.thisptr.rot = new_rot.q

    property size:
        def __get__(self):
            return self.thisptr.size

        def __set__(self, double new_size):
            self.thisptr.size = new_size

    property shape_id:
        def __get__(self):
            return self.shape_id

        def __set__(self, size_t val):
            self.shape_id = val

