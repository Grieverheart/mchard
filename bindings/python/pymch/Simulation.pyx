cimport mch
from Configuration cimport Configuration
from cython.operator cimport dereference as deref
from pymch.clam cimport Vec3d, Quatd

cdef class Simulation:
    cdef mch.Simulation* thisptr

    def __cinit__(self, Configuration config):
        self.thisptr = new mch.Simulation(deref(config.thisptr))

    def __dealloc__(self):
        del self.thisptr

    #MC Moves
    def rotate_particle(self, size_t pid, Quatd rotation):
        return self.thisptr.rotate_particle(pid, rotation.q)

    def move_particle(self, size_t pid, Vec3d dr):
        return self.thisptr.move_particle(pid, dr.v)

    def event_chain_move(self, size_t pid, double length, Vec3d r_dir):
        return self.thisptr.event_chain_move(pid, length, r_dir.v)

    def change_volume(self, double dv):
        return self.thisptr.change_volume(dv)

    def cluster_change_volume(self, double dv):
        return self.thisptr.cluster_change_volume(dv)

    #Getters
    #NOTE: How do we do this? We cannot return a reference!
    def get_configuration(self):
        cfg = Configuration()
        #Copy
        cfg.thisptr = new mch.Configuration(self.thisptr.get_configuration())
        return cfg

    def get_npart(self):
        return self.thisptr.get_npart()

    def get_pressure(self):
        return self.thisptr.get_pressure()

    def get_volume(self):
        return self.thisptr.get_volume()

    #Setters
    def set_pressure(self, double pressure):
        self.thisptr.set_pressure(pressure)

