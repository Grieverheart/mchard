from pymch cimport mch

cdef class Base:
    cdef mch.box.Base* thisptr

cdef class Rectangular:
    cdef mch.box.RectangularBox* thisptr

cdef class Ellipsoidal:
    cdef mch.box.EllipsoidalBox* thisptr
