cimport mch
from pymch.clam cimport Vec3d
from pymch.Particle cimport Particle
from pymch.shape.convex cimport *
from libcpp cimport bool
from libc.string cimport memcpy
from cython.operator cimport dereference as deref

cdef mch.shape.Variant* get_shape_variant(shape):
    if type(shape) is Polyhedron:
        return new mch.shape.Variant(deref((<Polyhedron>shape).thisptr))
    elif type(shape) is Sphere:
        return new mch.shape.Variant(deref((<Sphere>shape).thisptr))
    elif type(shape) is Cone:
        return new mch.shape.Variant(deref((<Cone>shape).thisptr))
    elif type(shape) is Bicone:
        return new mch.shape.Variant(deref((<Bicone>shape).thisptr))
    elif type(shape) is Cylinder:
        return new mch.shape.Variant(deref((<Cylinder>shape).thisptr))
    elif type(shape) is LeafCylinder:
        return new mch.shape.Variant(deref((<LeafCylinder>shape).thisptr))
    elif type(shape) is Box:
        return new mch.shape.Variant(deref((<Box>shape).thisptr))
    elif type(shape) is SphereSwept:
        return new mch.shape.Variant(deref((<SphereSwept>shape).thisptr))
    else:
        raise TypeError("Invalid shape type encountered: " + str(type(shape)))

cdef class Base:

    def __cinit__(self):
        self.thisptr = NULL

    def __dealloc__(self):
        del self.thisptr

    #@classmethod
    #def make_cubic(cls, double size, bool is_hard = False):
    #    ret = Base()
    #    ret.thisptr = new mch.box.CubicBox(size, is_hard)
    #    return ret

    def name(self):
        return self.thisptr.name()

    def bounds(self):
        ret = Vec3d()
        ret.v = self.thisptr.bounds()
        return ret

    def apply_pbc(self, Vec3d pos):
        ret = Vec3d()
        ret.v = self.thisptr.apply_pbc(pos.v)
        return ret

    def minimum_image(self, Vec3d pos):
        ret = Vec3d()
        ret.v = self.thisptr.minimum_image(pos.v)
        return ret

    def is_hard(self):
        return self.thisptr.is_hard()

    def volume(self):
        return self.thisptr.volume()

    def rescale(self, Vec3d factor):
        self.thisptr.rescale(factor.v)

    def check_overlap(self, Particle particle, shape):
        shape_ptr = get_shape_variant(shape)
        val = self.thisptr.check_overlap(deref(particle.thisptr), deref(shape_ptr))
        del shape_ptr
        return val

cdef class Rectangular:

    def __cinit__(self, Vec3d size, bool is_hard = False):
        self.thisptr = new mch.box.RectangularBox(size.v, is_hard)

    def __dealloc__(self):
        del self.thisptr

    def __copy__(self):
        ret = Rectangular(Vec3d(0.0))
        memcpy(ret.thisptr, self.thisptr, sizeof(mch.box.RectangularBox))
        return ret

    def size(self):
        ret = Vec3d()
        ret.v = self.thisptr.size()
        return ret

    def clone(self):
        ret = Base()
        ret.thisptr = self.thisptr.clone()
        return ret

    def name(self):
        return self.thisptr.name()

    def bounds(self):
        ret = Vec3d()
        ret.v = self.thisptr.bounds()
        return ret

    def apply_pbc(self, Vec3d pos):
        ret = Vec3d()
        ret.v = self.thisptr.apply_pbc(pos.v)
        return ret

    def minimum_image(self, Vec3d pos):
        ret = Vec3d()
        ret.v = self.thisptr.minimum_image(pos.v)
        return ret

    def is_hard(self):
        return self.thisptr.is_hard()

    def volume(self):
        return self.thisptr.volume()

    def rescale(self, Vec3d factor):
        self.thisptr.rescale(factor.v)

    def check_overlap(self, Particle particle, shape):
        shape_ptr = get_shape_variant(shape)
        val = self.thisptr.check_overlap(deref(particle.thisptr), deref(shape_ptr))
        del shape_ptr
        return val

cdef class Ellipsoidal:

    def __cinit__(self, Vec3d size):
        self.thisptr = new mch.box.EllipsoidalBox(size.v)

    def __dealloc__(self):
        del self.thisptr

    def __copy__(self):
        ret = Ellipsoidal(Vec3d(0.0))
        memcpy(ret.thisptr, self.thisptr, sizeof(mch.box.EllipsoidalBox))
        return ret

    def radius(self):
        ret = Vec3d()
        ret.v = self.thisptr.radius()
        return ret

    def clone(self):
        ret = Base()
        ret.thisptr = self.thisptr.clone()
        return ret

    def name(self):
        return self.thisptr.name()

    def bounds(self):
        ret = Vec3d()
        ret.v = self.thisptr.bounds()
        return ret

    def apply_pbc(self, Vec3d pos):
        ret = Vec3d()
        ret.v = self.thisptr.apply_pbc(pos.v)
        return ret

    def minimum_image(self, Vec3d pos):
        ret = Vec3d()
        ret.v = self.thisptr.minimum_image(pos.v)
        return ret

    def is_hard(self):
        return self.thisptr.is_hard()

    def volume(self):
        return self.thisptr.volume()

    def rescale(self, Vec3d factor):
        self.thisptr.rescale(factor.v)

    def check_overlap(self, Particle particle, shape):
        shape_ptr = get_shape_variant(shape)
        val = self.thisptr.check_overlap(deref(particle.thisptr), deref(shape_ptr))
        del shape_ptr
        return val

