from pymch cimport mch

cdef class Quatd:
    cdef mch.clam.Quat[double] q

cdef class Vec3d:
    cdef mch.clam.Vec3[double] v
