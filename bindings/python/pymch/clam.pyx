cimport mch

cdef class Vec3d:
    def __cinit__(self, *args):
        if len(args) > 3 or len(args) == 2:
            raise TypeError("Vec3d takes 0, 1 or 3 arguments (" + str(len(args)) + " given)")

        if len(args) == 3:
            self.v = mch.clam.Vec3[double](args[0], args[1], args[2])
        elif len(args) == 1:
            self.v = mch.clam.Vec3[double](<double>(args[0]))

    def __dealloc__(self):
        pass

    def __repr__(self):
        ret = "(" + str(self.v[0]) + ", " + str(self.v[1]) + ", " + str(self.v[2]) + ")"
        return ret

    def __getitem__(self, unsigned int idx):
        return self.v[idx]

    def __setitem__(self, unsigned int idx, double value):
        self.v[idx] = value

    def __richcmp__(Vec3d a, Vec3d b, int cmp_type):
        if cmp_type == 2:
            return (a.v == b.v)
        elif cmp_type == 3:
            return not (a.v == b.v)
        else:
            raise TypeError("Can only check equality.")

    def __mul__(Vec3d a, Vec3d b):
        ret = Vec3d()
        ret.v = a.v * b.v
        return ret

    def __div__(Vec3d a, Vec3d b):
        ret = Vec3d()
        ret.v = a.v / b.v
        return ret

    def __add__(Vec3d a, Vec3d b):
        ret = Vec3d()
        ret.v = a.v + b.v
        return ret

    def __sub__(Vec3d a, Vec3d b):
        ret = Vec3d()
        ret.v = a.v - b.v
        return ret

    def __iter__(self):
        for i in range(3):
            yield self.v[i]

    def length2(self):
        return self.v.length2()

    def length(self):
        return self.v.length()

    #TODO: copy operator
    #Vec3(const T*)

def cross(Vec3d a, Vec3d b):
    ret = Vec3d()
    ret.v = mch.clam.cross(a.v, b.v)
    return ret

def triple(Vec3d a, Vec3d b, Vec3d c):
    ret = Vec3d()
    ret.v = mch.clam.triple(a.v, b.v, c.v)
    return ret

def dot(Vec3d a, Vec3d b):
    return mch.clam.dot(a.v, b.v)

cdef class Quatd:
    def __cinit__(self, *args):
        if len(args) != 0 and len(args) != 4:
            raise TypeError("Quatd takes either 0, or 4 arguments (" + str(len(args)) + " given)")

        if len(args) == 4:
            self.q = mch.clam.Quat[double](args[0], args[1], args[2], args[3])

    def __dealloc__(self):
        pass

    def __repr__(self):
        ret = str(self.q[3]) + " + " + str(self.q[0]) + "i + " + str(self.q[1]) + "j + " + str(self.q[2]) + "k"
        return ret

    def __getitem__(self, unsigned int idx):
        return self.q[idx]

    def __setitem__(self, unsigned int idx, double value):
        self.q[idx] = value

    def __mul__(Quatd a, Quatd b):
        ret = Quatd()
        ret.q = a.q * b.q
        return ret

    def __add__(Quatd a, Quatd b):
        ret = Quatd()
        ret.q = a.q + b.q
        return ret

    def __sub__(Quatd a, Quatd b):
        ret = Quatd()
        ret.q = a.q - b.q
        return ret

    def __iter__(self):
        for i in range(4):
            yield self.q[i]

    def length2(self):
        return self.q.length2()

    def length(self):
        return self.q.length()

    def rotate(self, Vec3d rot):
        ret = Vec3d()
        ret.v = self.q.rotate(rot.v)
        return ret

    def toAxisAngle(self):
        cdef double angle = 0.0
        axis = Vec3d()
        self.q.toAxisAngle(angle, axis.v)
        return angle, axis

    def conj(self):
        ret = Quatd()
        ret.q = self.q.conj()
        return ret

    def inv(self):
        ret = Quatd()
        ret.q = self.q.inv()
        return ret

