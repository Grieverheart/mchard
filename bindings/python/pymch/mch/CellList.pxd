from clam cimport Vec3
from libcpp cimport bool

cdef extern from "MCH/CellList.h" namespace "MCH":
    ctypedef Vec3[double] Vec3d

    cdef cppclass CellList:
        CellList()
        CellList(const CellList&)

        void init(int nPart, Vec3d box_bounds, double minCellSize)
        int add(int pid, const Vec3d& pos)
        int update(int pid, const Vec3d& pos)
        int move(int pid, int coffset)
        void rescale(double factor)

        bool should_rebuild(double factor)const
        int cell_index(int pid)const
        int cell_index_at_offset(int cid, int offset)const
        Vec3d cell_size()const
        Vec3d cell_origin(int cid)const
        double cell_min_size()const

        CellIterator          cells()const
        CellNeighbourIterator cell_vol_nbs(int pid)const
        CellNeighbourIterator cell_nbs(int cid)const
        CellNeighbourIterator particle_cell_nbs(int pid)const
        CellNeighbourIterator particle_cell_nbs(const Vec3d& pos)const
        CellNeighbourIterator cell_dir_nbs(int cid, int direction)const
        CellContentIterator   cell_content(int cid)const

        void push_front(const CellNeighbourIterator&)
        int cellIndex(const Vec3d& pos)const;

    cdef cppclass CellIterator "MCH::CellList::CellIterator":
        CellIterator(int ncells)
        CellIterator(const CellIterator&)
        CellIterator begin()const
        CellIterator end()const

        bool operator!=(const CellIterator& other)const
        CellIterator& operator++()
        int operator*()const

    cdef cppclass CellNeighbourIterator "MCH::CellList::CellNeighbourIterator":
        CellNeighbourIterator(const int* neighbour_list, int n_neighbours)
        CellNeighbourIterator(const CellNeighbourIterator&)
        CellNeighbourIterator begin()const
        CellNeighbourIterator end()const
        bool operator!=(const CellNeighbourIterator& other)const
        CellNeighbourIterator& operator++()
        int operator*()const

    cdef cppclass CellContentIterator "MCH::CellList::CellContentIterator":
        CellContentIterator(const CellList& parent, int cidx)
        CellContentIterator(const CellContentIterator&)
        CellContentIterator begin()const
        CellContentIterator end()const
        bool operator!=(const CellContentIterator& other)const
        CellContentIterator& operator++()
        CellContentIterator operator+(int num)const
        int operator*()const

