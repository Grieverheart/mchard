cimport box
from Particle cimport Particle

cdef extern from "MCH/configuration.h" namespace "MCH":
    cdef cppclass Configuration:
        Configuration()
        Configuration(const box.Base*, int n_part, Particle* particles)
        Configuration(const Configuration&)

        size_t n_part_
        Particle* particles_
        box.Base* box_
