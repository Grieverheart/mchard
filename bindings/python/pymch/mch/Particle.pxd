cimport clam
from shape.variant cimport Variant

cdef extern from "MCH/particle.h" namespace "MCH":
    cdef cppclass Particle:
        Particle()
        Particle(const Particle&)
        double size
        clam.Vec3[double] pos
        clam.Quat[double] rot
        const Variant* shape
