cimport clam
from Configuration cimport Configuration
from libcpp cimport bool

cdef extern from "MCH/simulation.h" namespace "MCH":
    ctypedef clam.Vec3[double] Vec3d
    ctypedef clam.Quat[double] Quatd

    cdef cppclass Simulation:
        Simulation(const Configuration&)

        #MC Moves
        bool rotate_particle(size_t r_pid, const Quatd& rotation)
        bool move_particle(size_t r_pid, const Vec3d& dr)
        bool event_chain_move(size_t r_pid, double length, const Vec3d& r_dir)
        bool change_volume(double dv)
        bool cluster_change_volume(double dv)

        #Getters
        const Configuration& get_configuration()const
        size_t get_npart()const
        double get_pressure()const
        double get_volume()const

        #Setters
        void set_pressure(double)
