from Particle cimport Particle
from shape.variant cimport Variant
from clam.Vec3 cimport Vec3
from libcpp cimport bool

cdef extern from "MCH/box/Base.h" namespace "MCH::box":
    ctypedef Vec3[double] Vec3d
    cdef cppclass Base:
        Base* clone()const
        const char* name()const
        Vec3d bounds()const
        Vec3d apply_pbc(const Vec3d& pos)const
        Vec3d minimum_image(const Vec3d& pos)const
        bool check_overlap(const Particle&, const Variant&)const
        #bool raycast(const Particle&, const shape::Variant&, const clam::Vec3d& ray_dir, double& distance, clam::Vec3d& normal)const = 0;
        bool is_hard()const
        double volume()const
        void rescale(const Vec3d& factor)

cdef extern from "MCH/box/rectangular_box.h" namespace "MCH::box":
    ctypedef Vec3[double] Vec3d
    cdef cppclass RectangularBox(Base):
        RectangularBox(const Vec3d& size, bool is_hard)
        const Vec3d& size()const

cdef extern from "MCH/box/ellipsoidal_box.h" namespace "MCH::box":
    ctypedef Vec3[double] Vec3d
    cdef cppclass EllipsoidalBox(Base):
        EllipsoidalBox(const Vec3d& radius)
        const Vec3d& radius()const

cdef extern from * namespace "MCH::box":
    RectangularBox* dynamic_cast_rectangular "dynamic_cast<MCH::box::RectangularBox*>" (Base*)
    EllipsoidalBox* dynamic_cast_ellipsoidal "dynamic_cast<MCH::box::EllipsoidalBox*>" (Base*)
