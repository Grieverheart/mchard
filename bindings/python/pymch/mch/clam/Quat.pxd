from pymch cimport mch
from libcpp cimport bool
from pymch.mch.clam.Vec3 cimport Vec3

cdef extern from "MCH/clam/clam.h" namespace "clam":
    cdef cppclass Quat[T]:
        Quat()
        Quat(T, T, T, T)
        #Quat(const Vec3[T]&, T)
        Quat[T] conj()const
        T& operator[](unsigned int)
        Quat[T] operator*(const Quat[T]&)const
        #Quat[T] operator*(T)const
        #Quat[T] operator/(T)const
        Quat[T] operator+(const Quat[T]&)const
        Quat[T] operator-(const Quat[T]&)const
        Quat[T] inv()const
        T length2()const
        T length()const
        Vec3[T] rotate(const Vec3[T]&)const
        void toAxisAngle(T&, Vec3[T]&)const

    #Quat[T] uniform_quaternion[T, F](T max_angle, F gen_rand);
