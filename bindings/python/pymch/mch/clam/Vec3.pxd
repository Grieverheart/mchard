from libcpp cimport bool

cdef extern from "MCH/clam/clam.h" namespace "clam":
    cdef cppclass Vec3[T]:
        Vec3()
        Vec3(T)
        Vec3(T, T, T)
        Vec3(const T*)
        T& operator[](unsigned int)
        bool operator==(const Vec3[T]&)const
        Vec3[T] operator*(T)const
        Vec3[T] operator*(const Vec3[T]&)const
        Vec3[T] operator/(T)const
        Vec3[T] operator/(const Vec3[T]&)const
        Vec3[T] operator+(const Vec3[T]&)const
        Vec3[T] operator-(const Vec3[T]&)const
        Vec3[T] operator-()const
        T length2()const
        T length()const


    Vec3[T] cross[T](const Vec3[T]&, const Vec3[T]&)
    Vec3[T] triple[T](const Vec3[T]&, const Vec3[T]&, const Vec3[T]&)
    T dot[T](const Vec3[T]&, const Vec3[T]&)
    Vec3[T] operator*[T](T, const Vec3[T]&)
    Vec3[T] operator/[T](T, const Vec3[T]&)
