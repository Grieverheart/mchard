from convex_shape_definition cimport ConvexShapeDefinition
from ..clam.Vec3 cimport Vec3
from libcpp.vector cimport vector

cdef extern from "MCH/shape/convex.h" namespace "MCH::shape":
    ctypedef Vec3[double] Vec3d
    cdef cppclass Convex:
        Vec3d support(const Vec3d&)const
        double volume()const
        double in_radius()const
        double out_radius()const

cdef extern from "MCH/shape/polyhedron.h" namespace "MCH::shape":
    ctypedef Vec3[double] Vec3d
    cdef cppclass Polyhedron(Convex):
        Polyhedron(const vector[Vec3d]& vertices, const vector[vector[uint]]& faces, const char* source)
        Polyhedron(const Polyhedron&)
        double in_radius()const
        double out_radius()const

cdef extern from "MCH/shape/sphere.h" namespace "MCH::shape":
    cdef cppclass Sphere(Convex):
        Sphere()
        Sphere(const Sphere&)
        double radius()const

cdef extern from "MCH/shape/cone.h" namespace "MCH::shape":
    cdef cppclass Cone(Convex):
        Cone()
        Cone(double, double)
        Cone(const Cone&)
        double in_radius()const
        double out_radius()const
        double base_radius()const
        double height()const

cdef extern from "MCH/shape/bicone.h" namespace "MCH::shape":
    cdef cppclass Bicone(Convex):
        Bicone()
        Bicone(double, double)
        Bicone(const Bicone&)
        double in_radius()const
        double out_radius()const
        double base_radius()const
        double height()const

cdef extern from "MCH/shape/cylinder.h" namespace "MCH::shape":
    cdef cppclass Cylinder(Convex):
        Cylinder(double, double)
        Cylinder(const Cylinder&)
        double in_radius()const
        double out_radius()const

cdef extern from "MCH/shape/leaf_cylinder.h" namespace "MCH::shape":
    cdef cppclass LeafCylinder(Convex):
        LeafCylinder(double, double, double)
        LeafCylinder(const LeafCylinder&)
        double in_radius()const
        double out_radius()const

cdef extern from "MCH/shape/box.h" namespace "MCH::shape":
    ctypedef Vec3[double] Vec3d
    cdef cppclass Box(Convex):
        Box(const Vec3d&)
        Box(const Box&)
        double in_radius()const
        double out_radius()const

cdef extern from "MCH/shape/minkowski.h" namespace "MCH::shape":
    cdef cppclass Minkowski(Convex):
        Minkowski(const ConvexShapeDefinition&, const ConvexShapeDefinition&, double)
        Minkowski(const Minkowski&)
        double in_radius()const
        double out_radius()const

#NOTE: Not convex. Shouldn't be here.
cdef extern from "MCH/shape/complex.h" namespace "MCH::shape":
    cdef cppclass Complex:
        Complex(const ConvexShapeDefinition*, size_t)
        Complex(const Complex&)
        double in_radius()const
        double out_radius()const

cdef extern from "MCH/shape/hull.h" namespace "MCH::shape":
    cdef cppclass Hull(Convex):
        Hull(const ConvexShapeDefinition*, size_t, double)
        Hull(const Hull&)
        double in_radius()const
        double out_radius()const

cdef extern from "MCH/shape/sphere-swept.h" namespace "MCH::shape":
    cdef cppclass SphereSwept(Convex):
        SphereSwept(const Convex*, double radius)
        SphereSwept(const SphereSwept&)
        double in_radius()const
        double out_radius()const

