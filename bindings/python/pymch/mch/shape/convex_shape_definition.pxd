from .. cimport clam
from convex cimport Convex


cdef extern from "MCH/shape/convex_shape_definition.h" namespace "MCH::shape":
    cdef cppclass ConvexShapeDefinition:
        ConvexShapeDefinition()
        ConvexShapeDefinition(const ConvexShapeDefinition&)
        clam.Vec3[double] pos_;
        clam.Quat[double] rot_;
        double size_;
        const Convex* shape_;
