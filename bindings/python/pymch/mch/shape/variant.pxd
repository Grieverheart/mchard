from convex cimport *

cdef extern from "MCH/shape/variant.h" namespace "MCH::shape":
    cdef cppclass Variant:
        Variant(const Variant&);
        Variant(const Polyhedron&);
        Variant(const Sphere&);
        Variant(const Cone&);
        Variant(const Bicone&);
        Variant(const Cylinder&);
        Variant(const LeafCylinder&);
        Variant(const Box&);
        Variant(const Minkowski&);
        Variant(const Complex&);
        Variant(const Hull&);
        Variant(const SphereSwept&);

cdef extern from "<boost/variant/polymorphic_get.hpp>" namespace "MCH::shape":
    const Convex* dynamic_cast_convex "boost::polymorphic_get<MCH::shape::Convex>" (const Variant*)

cdef extern from * namespace "MCH::shape":
    const Polyhedron* dynamic_cast_polyhedron "boost::get<MCH::shape::Polyhedron>" (const Variant*)
    const Sphere* dynamic_cast_sphere "boost::get<MCH::shape::Sphere>" (const Variant*)
    const Cone* dynamic_cast_cone "boost::get<MCH::shape::Cone>" (const Variant*)
    const Bicone* dynamic_cast_bicone "boost::get<MCH::shape::Bicone>" (const Variant*)
    const Cylinder* dynamic_cast_cylinder "boost::get<MCH::shape::Cylinder>" (const Variant*)
    const LeafCylinder* dynamic_cast_leaf_cylinder "boost::get<MCH::shape::LeafCylinder>" (const Variant*)
    const Box* dynamic_cast_box "boost::get<MCH::shape::Box>" (const Variant*)
    const Minkowski* dynamic_cast_minkowski "boost::get<MCH::shape::Minkowski>" (const Variant*)
    const Complex* dynamic_cast_complex "boost::get<MCH::shape::Complex>" (const Variant*)
    const Hull* dynamic_cast_hull "boost::get<MCH::shape::Hull>" (const Variant*)
    const SphereSwept* dynamic_cast_sphere_swept "boost::get<MCH::shape::SphereSwept>" (const Variant*)
