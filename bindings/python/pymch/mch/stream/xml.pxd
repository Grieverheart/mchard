from ..Configuration cimport Configuration
from ..shape.variant cimport Variant
from libcpp cimport bool

cdef extern from "MCH/io/config_xml.h" namespace "MCH::io":
    bool xml_load_config(const char* filename, Configuration&, const Variant*** shapes, size_t* n_shapes)
    bool xml_save_config(const char* filename, const Configuration&, const Variant** shapes, size_t n_shapes)
