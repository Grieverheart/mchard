from pymch.mch.shape cimport convex
from pymch.mch.shape cimport Variant

cdef class Convex:
    cdef const Variant* thisptr

cdef class Polyhedron(Convex):
    pass

cdef class Sphere(Convex):
    pass

cdef class Cone(Convex):
    pass

cdef class Bicone(Convex):
    pass

cdef class Cylinder(Convex):
    pass

cdef class LeafCylinder(Convex):
    pass

cdef class Box(Convex):
    pass

cdef class Minkowski(Convex):
    pass

cdef class Hull(Convex):
    pass

cdef class SphereSwept(Convex):
    pass

cdef class Complex:
    cdef const Variant* thisptr
