from pymch cimport mch
from pymch.clam cimport Vec3d
from libcpp.vector cimport vector
from cython.operator cimport dereference as deref

#TODO: Implement Hull, Minkowski, and Complex shapes

cdef class Convex:

    def __dealloc__(self):
        del self.thisptr

    def support(self, Vec3d direction):
        ret = Vec3d()
        ret.v = mch.shape.dynamic_cast_convex(self.thisptr).support(direction.v)
        return ret

    def volume(self):
        return mch.shape.dynamic_cast_convex(self.thisptr).volume()

    def in_radius(self):
        return mch.shape.dynamic_cast_convex(self.thisptr).in_radius()

    def out_radius(self):
        return mch.shape.dynamic_cast_convex(self.thisptr).out_radius()


cdef class Polyhedron(Convex):

    def __cinit__(self, list vertices, list faces, const char* source = NULL):
        #convert list of vertices to std::vector
        cdef vector[mch.clam.Vec3[double]] cpp_vertices
        for vertex in vertices:
            cpp_vertices.push_back((<Vec3d>vertex).v)
        #convert list of faces to std::vector
        cdef vector[vector[uint]] cpp_faces
        cdef vector[uint] fids
        for face in faces:
            for fid in face:
                fids.push_back(fid)
            cpp_faces.push_back(fids)
            fids.clear()

        self.thisptr = new mch.shape.Variant(mch.shape.convex.Polyhedron(cpp_vertices, cpp_faces, source))


cdef class Sphere(Convex):

    def __cinit__(self):
        self.thisptr = new mch.shape.Variant(mch.shape.convex.Sphere())

    def radius(self):
        return mch.shape.dynamic_cast_sphere(self.thisptr).radius()

cdef class Cone(Convex):

    def __cinit__(self, double base_radius, double height):
        self.thisptr = new mch.shape.Variant(mch.shape.convex.Cone(base_radius, height))

    def base_radius(self):
        return mch.shape.dynamic_cast_cone(self.thisptr).base_radius()

    def height(self):
        return mch.shape.dynamic_cast_cone(self.thisptr).height()

cdef class Bicone(Convex):

    def __cinit__(self, double base_radius, double height):
        self.thisptr = new mch.shape.Variant(mch.shape.convex.Bicone(base_radius, height))

    def base_radius(self):
        return mch.shape.dynamic_cast_bicone(self.thisptr).base_radius()

    def height(self):
        return mch.shape.dynamic_cast_bicone(self.thisptr).height()

cdef class Cylinder(Convex):

    def __cinit__(self, double base_radius, double height):
        self.thisptr = new mch.shape.Variant(mch.shape.convex.Cylinder(base_radius, height))

    def support(self, Vec3d direction):
        ret = Vec3d()
        ret.v = mch.shape.dynamic_cast_cylinder(self.thisptr).support(direction.v)
        return ret

cdef class LeafCylinder(Convex):

    def __cinit__(self, double width, double length, double height):
        self.thisptr = new mch.shape.Variant(mch.shape.convex.LeafCylinder(width, length, height))

    def support(self, Vec3d direction):
        ret = Vec3d()
        ret.v = mch.shape.dynamic_cast_leaf_cylinder(self.thisptr).support(direction.v)
        return ret

cdef class Box(Convex):

    def __cinit__(self, Vec3d size):
        self.thisptr = new mch.shape.Variant(mch.shape.convex.Box(size.v))


cdef class SphereSwept(Convex):

    def __cinit__(self, Convex shape, double radius):
        self.thisptr = new mch.shape.Variant(mch.shape.convex.SphereSwept(mch.shape.dynamic_cast_convex(shape.thisptr), radius))

