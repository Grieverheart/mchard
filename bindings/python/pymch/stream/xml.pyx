from pymch cimport mch
from pymch.clam cimport Vec3d
from pymch.shape.convex cimport *
from pymch.Configuration cimport Configuration
from cython.operator cimport dereference as deref
from libcpp cimport bool
from libcpp.vector cimport vector
from libc.stdlib cimport malloc, free

cdef extern from *: 
    void array_delete 'delete[]' (const mch.shape.Variant**) 

#TODO: Implement Hull, Complex, Minkowski
def load_config(const char* filename):
    config = Configuration()
    config.thisptr = new mch.Configuration()
    config.shapes = []

    cdef const mch.shape.Variant** shapes_ptr
    cdef size_t n_shapes

    if mch.stream.xml_load_config(filename, deref(config.thisptr), &shapes_ptr, &n_shapes):
        for i in range(n_shapes):
            if mch.shape.dynamic_cast_polyhedron(shapes_ptr[i]) is not NULL:
                p_shape = Polyhedron([], [])
                del (<Polyhedron>p_shape).thisptr
                (<Polyhedron>p_shape).thisptr = shapes_ptr[i]
                config.shapes.append(p_shape)
                continue

            if mch.shape.dynamic_cast_sphere(shapes_ptr[i]) is not NULL:
                p_shape = Sphere()
                del p_shape.thisptr
                (<Sphere>p_shape).thisptr = shapes_ptr[i]
                config.shapes.append(p_shape)
                continue

            if mch.shape.dynamic_cast_cone(shapes_ptr[i]) is not NULL:
                p_shape = Cone(0.0, 0.0)
                del (<Cone>p_shape).thisptr
                (<Cone>p_shape).thisptr = shapes_ptr[i]
                config.shapes.append(p_shape)
                continue

            if mch.shape.dynamic_cast_cylinder(shapes_ptr[i]) is not NULL:
                p_shape = Cylinder(0.0, 0.0)
                del (<Cylinder>p_shape).thisptr
                (<Cylinder>p_shape).thisptr = shapes_ptr[i]
                config.shapes.append(p_shape)
                continue

            if mch.shape.dynamic_cast_leaf_cylinder(shapes_ptr[i]) is not NULL:
                p_shape = LeafCylinder(0.0, 0.0, 0.0)
                del (<LeafCylinder>p_shape).thisptr
                (<LeafCylinder>p_shape).thisptr = shapes_ptr[i]
                config.shapes.append(p_shape)
                continue

            if mch.shape.dynamic_cast_box(shapes_ptr[i]) is not NULL:
                p_shape = Box(Vec3d(0.0))
                del (<Box>p_shape).thisptr
                (<Box>p_shape).thisptr = shapes_ptr[i]
                config.shapes.append(p_shape)
                continue

            if mch.shape.dynamic_cast_bicone(shapes_ptr[i]) is not NULL:
                p_shape = Bicone(0.0, 0.0)
                del (<Bicone>p_shape).thisptr
                (<Bicone>p_shape).thisptr = shapes_ptr[i]
                config.shapes.append(p_shape)
                continue

            raise TypeError("Unknown shape type found.")

        array_delete(shapes_ptr)

        return config
    else:
        raise IOError("Error reading configuration " + filename)

def save_config(const char* filename, Configuration config):
    assert(len(config.shapes) > 0)
    shapes_ptr = <const mch.shape.Variant**>malloc(len(config.shapes) * sizeof(mch.shape.Variant*))

    for sid in range(len(config.shapes)):
        shapes_ptr[sid] = (<Convex>config.shapes[sid]).thisptr

    ret_val = mch.stream.xml_save_config(filename, deref(config.thisptr), shapes_ptr, len(config.shapes))
    free(shapes_ptr)
    if not ret_val:
        raise IOError("Error when saving configuration " + filename)

