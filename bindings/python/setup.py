from distutils.core import setup
from distutils.extension import Extension
from Cython.Build import cythonize

extensions = [
    Extension(
        "*",
        ["pymch/*.pyx"],
        libraries = ["MCH"],
        #library_dirs = ["../../build/"],
        #runtime_library_dirs = ["../../build/"],
        extra_compile_args = ["-std=c++11"],
        language = "c++",
    ),
    Extension(
        "pymch.stream.xml",
        ["pymch/stream/xml.pyx"],
        libraries = ["MCH"],
        #library_dirs = ["../../build/"],
        #runtime_library_dirs = ["../../build/"],
        extra_compile_args = ["-std=c++11"],
        language = "c++",
    ),
    Extension(
        "pymch.shape.*",
        ["pymch/shape/*.pyx"],
        libraries = ["MCH"],
        #library_dirs = ["../../build/"],
        #runtime_library_dirs = ["../../build/"],
        extra_compile_args = ["-std=c++11"],
        language = "c++",
    ),
]

setup(
    name = "pymch",
    packages = ["pymch", "pymch.stream", "pymch.shape"],
    include_dirs = ["../../include", "../../external/dSFMT"],
    ext_modules = cythonize(extensions)
)
