import pymch as mch

if __name__ == '__main__':

    try:
        config = mch.stream.xml.load_config("configs/test.xml")
    except:
        quit()

    sim = mch.Simulation(config)

    #fp = open("Data/pf.dat", "a")

    dx_max     = 0.1
    dtheta_max = 0.2
    dv_max     = 40.0
    pressure   = 1.2

    for p in range(1):
        print pressure
        sim.set_pressure(pressure)

        accepted_rot = 0
        accepted_vol = 0
        attempted = 0
        #Equilibration run
        vol_av = 0.0

        for steps in range(100):
            print steps

            for i in range(sim.get_npart()):
                accepted_rot += sim.rotate_particle(dtheta_max);
                sim.event_chain_move(dx_max);
                attempted += 1

            accepted_vol += sim.cluster_change_volume(dv_max);
            vol_av += sim.get_volume();

            if (steps + 1) % 100 == 0:
                if float(accepted_rot) / attempted > 0.3:
                    if dtheta_max < 0.5:
                        dtheta_max *= 1.1;
                else:
                    dtheta_max *= 0.9;

                print float(accepted_vol) / (attempted / sim.get_npart()), sim.get_npart() / sim.get_volume()

                if float(accepted_vol) / (attempted / sim.get_npart()) > 0.2:
                    dv_max *= 1.1;
                else:
                    dv_max *= 0.9;

                accepted_rot = 0
                accepted_vol = 0
                attempted    = 0

                #f.write()
                #fprintf(fp, "%lu\t%f\t%f\t%f\n", time(NULL) - start_time, (100.0 * sim.get_npart()) / vol_av, dtheta_max, dv_max);
                #fflush(fp);

                vol_av = 0.0

                #char filename[128];
                #sprintf(filename, "Data/out.p%.2f.step%06lu.xml", pressure, steps);
                #io::xml_save_config(filename, sim.get_configuration());

        #Production run
        #for(size_t steps = 0; steps < 10000; ++steps){
        #    for(size_t i = 0; i < sim.get_npart(); ++i){
        #        sim.rotate_particle(dtheta_max);
        #        sim.event_chain_move(dx_max);
        #    }

        #    if((steps + 1) % 1000 == 0){
        #        char filename[128];
        #        sprintf(filename, "Data/out.p%.2f.step%06lu.dat", pressure, steps);
        #        save_config(filename, sim.get_configuration(), shape_names);
        #    }
        #}

        pressure += 0.1 * pressure

