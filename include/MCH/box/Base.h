#ifndef MCH_BOX_BASE_H
#define MCH_BOX_BASE_H

#ifdef __cplusplus

#include "MCH/clam/clam.h"
#include "MCH/shape/variant_fwd.h"

namespace MCH{

    struct Particle;

    namespace box{

        class Base{
        public:
            virtual ~Base(void){};
            virtual Base* clone(void)const = 0;
            virtual const char* name(void)const = 0;
            virtual clam::Vec3d bounds(void)const = 0;
            virtual clam::Vec3d apply_pbc(const clam::Vec3d& pos)const = 0;
            virtual clam::Vec3d minimum_image(const clam::Vec3d& pos)const = 0;
            virtual clam::Vec3d minimum_image_vector(const clam::Vec3d& dist)const = 0;
            virtual bool check_overlap(const Particle&, const shape::Variant&)const = 0;
            virtual bool raycast(const Particle&, const shape::Variant&, const clam::Vec3d& ray_dir, double& distance, clam::Vec3d& normal)const = 0;
            virtual bool is_hard(void)const = 0;
            virtual double volume(void)const = 0;
            virtual void rescale(const clam::Vec3d& factor) = 0;
        };

    }
}

using MchBoxBase = MCH::box::Base;

#else
typedef struct MchBoxBase MchBoxBase;
#endif

#ifdef __cplusplus
using MchParticle = MCH::Particle;
extern "C" {
#endif
    void mch_box_base_delete(MchBoxBase*);
    MchBoxBase* mch_box_base_clone(const MchBoxBase*);
    const char* mch_box_base_name(const MchBoxBase*);
    void mch_box_base_bounds(const MchBoxBase*, double* bounds);
    void mch_box_base_apply_pbc(const MchBoxBase*, const double* pos, double* new_pos);
    void mch_box_base_minimum_image(const MchBoxBase*, const double* pos, double* new_pos);
    bool mch_box_base_check_overlap(const MchBoxBase*, const MchParticle*, const MchShapeVariant*);
    bool mch_box_base_raycast(const MchBoxBase*, const MchParticle*, const MchShapeVariant*, const double* ray_dir, double* distance, double* normal);
    bool mch_box_base_is_hard(const MchBoxBase*);
    double mch_box_base_volume(const MchBoxBase*);
    void mch_box_base_rescale(MchBoxBase*, double* factor);
#ifdef __cplusplus
}
#endif

#endif
