#ifndef MCH_BOX_ELLIPSOIDAL_BOX_H
#define MCH_BOX_ELLIPSOIDAL_BOX_H

#include "MCH/box/Base.h"

#ifdef __cplusplus

namespace MCH{
    namespace box{

        class EllipsoidalBox: public Base{
        public:
            EllipsoidalBox(const clam::Vec3d& radius);
            const clam::Vec3d& radius(void)const{
                return radius_;
            }
            Base* clone(void)const;
            const char* name(void)const;
            clam::Vec3d bounds(void)const;
            clam::Vec3d apply_pbc(const clam::Vec3d& pos)const;
            clam::Vec3d minimum_image(const clam::Vec3d& pos)const;
            clam::Vec3d minimum_image_vector(const clam::Vec3d& dist)const;
            bool check_overlap(const Particle&, const shape::Variant&)const;
            bool raycast(const Particle&, const shape::Variant&, const clam::Vec3d& ray_dir, double& distance, clam::Vec3d& normal)const;
            bool is_hard(void)const;
            double volume(void)const;
            void rescale(const clam::Vec3d& factor);
        private:
            class ShapeOverlapVisitor;
            class ShapeRaycastVisitor;
            clam::Vec3d radius_;
        };

    }
}
#endif

#ifdef __cplusplus
extern "C" {
#endif
    //We don't need derived, so case to base
    MchBoxBase* mch_box_make_ellipsoidal_box(double radius);
#ifdef __cplusplus
}
#endif

#endif
