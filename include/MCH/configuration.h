#ifndef MCH_CONFIGURATION_H
#define MCH_CONFIGURATION_H

#include <cstddef>

#ifdef __cplusplus
namespace MCH{

    struct Particle;

    namespace box{
        class Base;
    }

    //TODO: I don't like the way Configuration is set up. Perhaps we'll
    //make the variables private so that the user doesn't have to deal
    //with allocations. Also make Simulation a friend of configuration.
    struct Configuration{
        Configuration(void);
        Configuration(const box::Base*, int n_part, const Particle* particles);
        Configuration(const Configuration&);

        ~Configuration(void);

        size_t n_part_;
        Particle* particles_;
        box::Base* box_;
    };
}
#else
    typedef struct MchParticle MchParticle;
    typedef struct MchBoxBase MchBoxBase;

    typedef struct{
        size_t n_part_;
        MchParticle* particles_;
        MchBoxBase* box_;
    }MchConfiguration;
#endif// __cplusplus

#ifdef __cplusplus
using MchConfiguration = MCH::Configuration;
extern "C" {
#endif
    MchConfiguration* mch_config_new(void);
    void mch_config_delete(MchConfiguration*);
#ifdef __cplusplus
}
#endif

#endif
