#ifndef MCH_IO_XML_H
#define MCH_IO_XML_H

#include "MCH/configuration.h"
#include "MCH/shape/variant_fwd.h"

#ifdef __cplusplus
namespace MCH{
    namespace io{

        bool xml_load_config(const char* filename, Configuration&, const shape::Variant*** shapes, size_t* n_shapes);
        bool xml_save_config(const char* filename, const Configuration&, const shape::Variant** shapes, size_t n_shapes);

    }
}
#endif// __cplusplus


#ifdef __cplusplus
extern "C" {
#endif
    bool mch_io_xml_load_config(const char* filename, MchConfiguration*, const MchShapeVariant***, size_t* n_shapes);
    bool mch_io_xml_save_config(const char* filename, const MchConfiguration*, const MchShapeVariant**, size_t n_shapes);
#ifdef __cplusplus
}
#endif

#endif
