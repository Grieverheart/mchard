#ifndef MCH_OVERLAP_OVERLAP_H
#define MCH_OVERLAP_OVERLAP_H

#include "MCH/shape/variant_fwd.h"
#include "MCH/clam/clam.h"

#ifdef __cplusplus
namespace MCH{

    struct Particle;

    namespace overlap{

        //TODO: return clam::Vec3d instead.
        clam::Vec3d shape_distance(const Particle&, const shape::Variant&, const Particle&, const shape::Variant&);
        bool shape_overlap(const Particle&, const shape::Variant&, const Particle&, const shape::Variant&, double feather = 0.0);
        bool shape_raycast(const Particle&, const shape::Variant&, const Particle&, const shape::Variant&, const clam::Vec3d& ray_dir, double& distance, clam::Vec3d& normal);

    }
}
#else
typedef struct MchParticle MchParticle;
#endif

#ifdef __cplusplus
using MchParticle = MCH::Particle;
extern "C"{
#endif
    void mch_shape_distance(const MchParticle*, const MchShapeVariant*, const MchParticle*, const MchShapeVariant*, double* distance);
    bool mch_shape_overlap(const MchParticle*, const MchShapeVariant*, const MchParticle*, const MchShapeVariant*, double feather);
    bool mch_shape_raycast(const MchParticle*, const MchShapeVariant*, const MchParticle*, const MchShapeVariant*, const double* ray_dir, double* distance, double* normal);
#ifdef __cplusplus
}
#endif

#endif
