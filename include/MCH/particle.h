#ifndef MC_HARD_PARTICLE_H
#define MC_HARD_PARTICLE_H

#ifdef __cplusplus
#include <cstddef>
#else
#include <stddef.h>
#endif

#ifdef __cplusplus

#include "clam/clam.h"
#include "shape/variant_fwd.h"

namespace MCH{

    struct Particle{
        double      size;
        clam::Vec3d pos;
        clam::Quatd rot;
        const shape::Variant* shape;
    };

}

using MchParticle = MCH::Particle;

#else
    typedef struct{
        double size;
        double pos[3];
        double rot[4];
        //TODO: Change this to i.e. uint_fast32_t
        size_t shape_id;
    }MchParticle;
#endif

#endif
