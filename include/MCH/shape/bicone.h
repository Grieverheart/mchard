#ifndef MCH_SHAPE_BICONE_H
#define MCH_SHAPE_BICONE_H

#ifdef __cplusplus

#include "convex.h"

namespace MCH{
    namespace shape{

        class Bicone: public Convex{
        public:
            explicit Bicone(double base_radius, double height);

            double in_radius(void)const{
                return in_radius_;
            }
            double out_radius(void)const{
                return out_radius_;
            }
            double volume(void)const{
                return volume_;
            }
            double base_radius(void)const{
                return base_radius_;
            }
            double height(void)const{
                return 2.0 * half_height_;
            }

            clam::Vec3d support(const clam::Vec3d&)const;
        private:
            double base_radius_, half_height_;
            double sintheta_;
            double in_radius_, out_radius_;
            double volume_;
        };

        inline clam::Vec3d Bicone::support(const clam::Vec3d& dir)const{
            double test = dir[1] / dir.length();
            if(test >= sintheta_) return clam::Vec3d(0.0, half_height_, 0.0);
            else if(test <= -sintheta_) return clam::Vec3d(0.0, -half_height_, 0.0);
            else{
                double factor = base_radius_ / sqrt(dir[0] * dir[0] + dir[2] * dir[2]);
                return clam::Vec3d(factor * dir[0], 0.0, factor * dir[2]);
            }
        }

    }
}
#else
typedef struct MchShapeBicone MchShapeBicone;
#endif

#ifdef __cplusplus
using MchShapeBicone = MCH::shape::Bicone;
extern "C" {
#endif
    MchShapeBicone* mch_shape_make_bicone(double base_radius, double height);
    void mch_shape_delete_bicone(MchShapeBicone*);

    //double in_radius(void)const;
    //double out_radius(void)const;
    //double volume(void)const;
    //const std::vector<clam::Vec3d>& vertices(void)const;
    //const std::vector<std::vector<unsigned int>>& faces(void)const;
    //const clam::Vec3d& get_vertex(size_t idx)const;
    //const std::vector<unsigned int>& get_vertex_nbs(size_t idx)const;

    //clam::Vec3d support(const clam::Vec3d&)const;
    //double max_vert_dist2(const clam::Vec3d& pos, const clam::Quatd& rot)const;

#ifdef __cplusplus
}
#endif

#endif
