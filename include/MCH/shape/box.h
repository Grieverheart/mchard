#ifndef MCH_SHAPE_BOX_H
#define MCH_SHAPE_BOX_H

#ifdef __cplusplus

#include "convex.h"
#include <cmath>

namespace MCH{
    namespace shape{

        class Box: public Convex{
        public:
            Box(const clam::Vec3d& dims);

            double in_radius(void)const{
                return in_radius_;
            }
            double out_radius(void)const{
                return out_radius_;
            }
            double volume(void)const{
                return volume_;
            }
            clam::Vec3d extent(void)const{
                return extent_;
            }

            clam::Vec3d support(const clam::Vec3d&)const;
        private:
            clam::Vec3d extent_;
            double in_radius_, out_radius_;
            double volume_;
        };

        inline clam::Vec3d Box::support(const clam::Vec3d& dir)const{
            return clam::Vec3d(
                std::copysign(extent_[0], dir[0]),
                std::copysign(extent_[1], dir[1]),
                std::copysign(extent_[2], dir[2])
            );
        }

    }
}
#else
typedef struct MchShapeBox MchShapeBox;
#endif

#ifdef __cplusplus
using MchShapeBox = MCH::shape::Box;
extern "C" {
#endif
    MchShapeBox* mch_shape_make_box(double hx, double hy, double hz);
    void mch_shape_delete_box(MchShapeBox*);

    //double in_radius(void)const;
    //double out_radius(void)const;
    //double volume(void)const;
    //const std::vector<clam::Vec3d>& vertices(void)const;
    //const std::vector<std::vector<unsigned int>>& faces(void)const;
    //const clam::Vec3d& get_vertex(size_t idx)const;
    //const std::vector<unsigned int>& get_vertex_nbs(size_t idx)const;

    //clam::Vec3d support(const clam::Vec3d&)const;
    //double max_vert_dist2(const clam::Vec3d& pos, const clam::Quatd& rot)const;

#ifdef __cplusplus
}
#endif

#endif
