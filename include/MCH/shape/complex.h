#ifndef MCH_SHAPE_COMPLEX_H
#define MCH_SHAPE_COMPLEX_H

#include "convex_shape_definition.h"
#include <cstddef>

namespace MCH{
    namespace shape{

        class Complex{
        public:
            Complex(const ConvexShapeDefinition* shape_defs, size_t n_shape_defs);
            Complex(const Complex& other);
            ~Complex(void);

            const ConvexShapeDefinition* shape_definitions(void)const;
            const size_t num_shape_definitions(void)const;

            double in_radius(void)const;
            double out_radius(void)const;
            double volume(void)const;

        private:
            ConvexShapeDefinition* shape_defs_;
            size_t n_shape_defs_;
            double out_radius_;
            double volume_;
        };

    }
}

#endif
