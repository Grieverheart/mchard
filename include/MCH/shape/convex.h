#ifndef MCH_SHAPE_CONVEX_H
#define MCH_SHAPE_CONVEX_H

#ifdef __cplusplus
#include "MCH/clam/clam.h"

namespace MCH{
    namespace shape{

        class Convex{
        public:
            virtual ~Convex(void){};
            virtual clam::Vec3d support(const clam::Vec3d&)const = 0;
            virtual double volume(void)const = 0;
            virtual double in_radius(void)const = 0;
            virtual double out_radius(void)const = 0;
        };

    }
}
#endif

#endif
