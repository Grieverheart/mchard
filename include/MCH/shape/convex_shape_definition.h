#ifndef MCH_SHAPE_CONVEX_SHAPE_DEF_H
#define MCH_SHAPE_CONVEX_SHAPE_DEF_H

#include "MCH/clam/clam.h"

//TODO: C++

namespace MCH{
    namespace shape{
        class Convex;
        struct ConvexShapeDefinition{
            clam::Vec3d pos_;
            clam::Quatd rot_;
            double size_;
            const Convex* shape_;
        };
    }
}

#endif
