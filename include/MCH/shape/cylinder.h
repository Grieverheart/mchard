#ifndef MCH_SHAPE_CYLINDER_H
#define MCH_SHAPE_CYLINDER_H

#ifdef __cplusplus

#include "convex.h"
#include <cmath>

namespace MCH{
    namespace shape{

        class Cylinder: public Convex{
        public:
            explicit Cylinder(double base_radius, double height);

            double in_radius(void)const{
                return in_radius_;
            }
            double out_radius(void)const{
                return out_radius_;
            }
            double volume(void)const{
                return volume_;
            }
            double base_radius(void)const{
                return base_radius_;
            }
            double height(void)const{
                return 2.0 * half_height_;
            }

            clam::Vec3d support(const clam::Vec3d&)const;
        private:
            double base_radius_, half_height_;
            double in_radius_, out_radius_;
            double volume_;
        };

        inline clam::Vec3d Cylinder::support(const clam::Vec3d& dir)const{
            double length = sqrt(dir[0] * dir[0] + dir[2] * dir[2]);
            if(length != 0.0){
                double d = base_radius_ / length;
                return clam::Vec3d(d * dir[0], std::copysign(half_height_, dir[1]), d * dir[2]);
            }
            else return clam::Vec3d(base_radius_, std::copysign(half_height_, dir[1]), 0.0);
        }

    }
}
#else
typedef struct MchShapeCylinder MchShapeCylinder;
#endif

#ifdef __cplusplus
using MchShapeCylinder = MCH::shape::Cylinder;
extern "C" {
#endif
    MchShapeCylinder* mch_shape_make_cylinder(double base_radius, double height);
    void mch_shape_delete_cylinder(MchShapeCylinder*);

    //double in_radius(void)const;
    //double out_radius(void)const;
    //double volume(void)const;
    //const std::vector<clam::Vec3d>& vertices(void)const;
    //const std::vector<std::vector<unsigned int>>& faces(void)const;
    //const clam::Vec3d& get_vertex(size_t idx)const;
    //const std::vector<unsigned int>& get_vertex_nbs(size_t idx)const;

    //clam::Vec3d support(const clam::Vec3d&)const;
    //double max_vert_dist2(const clam::Vec3d& pos, const clam::Quatd& rot)const;

#ifdef __cplusplus
}
#endif

#endif
