#ifndef MCH_SHAPE_HULL_H
#define MCH_SHAPE_HULL_H

//TODO: C bindings

#include "convex.h"
#include "convex_shape_definition.h"
#include <cstddef>

namespace MCH{
    namespace shape{

        class Hull: public Convex{
        public:
            Hull(const ConvexShapeDefinition* shapes, size_t n_shapes, double volume = 1.0);
            Hull(const Hull& other);
            ~Hull(void);

            clam::Vec3d support(const clam::Vec3d& dir)const{
                clam::Vec3d ret = shape_defs_[0].pos_ + shape_defs_[0].rot_.rotate(shape_defs_[0].size_ * shape_defs_[0].shape_->support(shape_defs_[0].rot_.inv().rotate(dir)));
                double max = clam::dot(ret, dir);
                for(size_t i = 1; i < n_shape_defs_; ++i){
                    auto sup = shape_defs_[i].pos_ + shape_defs_[i].rot_.rotate(shape_defs_[i].size_ * shape_defs_[i].shape_->support(shape_defs_[i].rot_.inv().rotate(dir)));
                    double dot = clam::dot(sup, dir);
                    if(dot > max){
                        max = dot;
                        ret = sup;
                    }
                }

                return ret;
            }

            const ConvexShapeDefinition* shape_definitions(void)const;
            const size_t num_shape_definitions(void)const;
            double in_radius(void)const;
            double out_radius(void)const;
            double volume(void)const;

        private:
            ConvexShapeDefinition* shape_defs_;
            size_t n_shape_defs_;
            double out_radius_;
            double volume_;
        };

    }
}

#endif
