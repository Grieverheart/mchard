#ifndef MCH_SHAPE_LEAF_CYLINDER_H
#define MCH_SHAPE_LEAF_CYLINDER_H

#ifdef __cplusplus

#include "convex.h"
#include <cmath>

namespace MCH{
    namespace shape{

        //NOTE: w < l!
        /* Vesica Piscis */
        class LeafCylinder: public Convex{
        public:
            LeafCylinder(void){}

            explicit LeafCylinder(double width, double length, double height);

            double in_radius(void)const{
                return in_radius_;
            }
            double out_radius(void)const{
                return out_radius_;
            }
            double volume(void)const{
                return volume_;
            }
            double width(void)const{
                return 2.0 * half_width_;
            }
            double length(void)const{
                return 2.0 * half_length_;
            }
            double height(void)const{
                return 2.0 * half_height_;
            }

            clam::Vec3d support(const clam::Vec3d&)const;
        private:
            double half_width_, half_length_, half_height_;
            double circle_radius_, circle_distance_;
            double in_radius_, out_radius_;
            double volume_;
        };

        inline clam::Vec3d LeafCylinder::support(const clam::Vec3d& dir)const{
            double x = 0.0, z = 0.0;
            if(dir[0] != 0.0 || dir[2] != 0.0){
                double l = sqrt(dir[0] * dir[0] + dir[2] * dir[2]);
                double test = dir[2] / l;
                if(test >= half_length_ / circle_radius_) z = half_length_;
                else if(test <= -half_length_ / circle_radius_) z = -half_length_;
                else{
                    x = circle_radius_ * dir[0] / l - std::copysign(circle_distance_, dir[0]);
                    z = circle_radius_ * dir[2] / l;
                }
            }

            return clam::Vec3d(x, std::copysign(half_height_, dir[1]), z);
        }

    }
}
#else
typedef struct MchShapeLeafCylinder MchShapeLeafCylinder;
#endif

#ifdef __cplusplus
using MchShapeLeafCylinder = MCH::shape::LeafCylinder;
extern "C" {
#endif
    MchShapeLeafCylinder* mch_shape_make_leaf_cylinder(double base_radius, double height);
    void mch_shape_delete_leaf_cylinder(MchShapeLeafCylinder*);

    //double in_radius(void)const;
    //double out_radius(void)const;
    //double volume(void)const;
    //const std::vector<clam::Vec3d>& vertices(void)const;
    //const std::vector<std::vector<unsigned int>>& faces(void)const;
    //const clam::Vec3d& get_vertex(size_t idx)const;
    //const std::vector<unsigned int>& get_vertex_nbs(size_t idx)const;

    //clam::Vec3d support(const clam::Vec3d&)const;
    //double max_vert_dist2(const clam::Vec3d& pos, const clam::Quatd& rot)const;

#ifdef __cplusplus
}
#endif

#endif
