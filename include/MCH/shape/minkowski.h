#ifndef MCH_SHAPE_MINKOWSKI_H
#define MCH_SHAPE_MINKOWSKI_H

#ifdef __cplusplus

#include "convex.h"
#include "convex_shape_definition.h"

namespace MCH{
    namespace shape{

        class Minkowski: public Convex{
        public:
            Minkowski(const ConvexShapeDefinition& pa, const ConvexShapeDefinition& pb, double volume = 1.0);

            clam::Vec3d support(const clam::Vec3d& dir)const{
                return pa_.pos_ + pa_.size_ * pa_.rot_.rotate(pa_.shape_->support(inv_rot_a_.rotate(dir))) +
                       pb_.pos_ + pb_.size_ * pb_.rot_.rotate(pb_.shape_->support(inv_rot_b_.rotate(dir)));
            }

            double in_radius(void)const;
            double out_radius(void)const;
            double volume(void)const;
            const ConvexShapeDefinition& shape_def_a(void)const;
            const ConvexShapeDefinition& shape_def_b(void)const;

        private:
            ConvexShapeDefinition pa_, pb_;
            clam::Quatd inv_rot_a_, inv_rot_b_;
            double in_radius_, out_radius_;
            double volume_;
        };

    }
}
#else
typedef struct MchShapeMinkowski MchShapeMinkowski;
#endif

//TODO: Implement these.
//#ifdef __cplusplus
//using MchShapeMinkowski = MCH::shape::Minkowski;
//extern "C" {
//#endif
//    MchShapeMinkowski* mch_shape_make_cone(double base_radius, double height);
//    void mch_shape_delete_cone(MchShapeMinkowski*);
//
//    //double in_radius(void)const;
//    //double out_radius(void)const;
//    //double volume(void)const;
//    //const std::vector<clam::Vec3d>& vertices(void)const;
//    //const std::vector<std::vector<unsigned int>>& faces(void)const;
//    //const clam::Vec3d& get_vertex(size_t idx)const;
//    //const std::vector<unsigned int>& get_vertex_nbs(size_t idx)const;
//
//    //clam::Vec3d support(const clam::Vec3d&)const;
//    //double max_vert_dist2(const clam::Vec3d& pos, const clam::Quatd& rot)const;
//
//#ifdef __cplusplus
//}
//#endif

#endif
