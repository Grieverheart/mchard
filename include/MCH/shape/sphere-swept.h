#ifndef MCH_SHAPE_SPHERE_SWEPT_H
#define MCH_SHAPE_SPHERE_SWEPT_H

#ifdef __cplusplus

#include "convex.h"

namespace MCH{
    namespace shape{

        class SphereSwept: public Convex{
        public:
            SphereSwept(const Convex* shape, double radius);

            clam::Vec3d support(const clam::Vec3d& dir)const{
                double norm = dir.length();
                return shape_->support(dir) + ((norm > 0.0)? dir * (radius_ / norm): clam::Vec3d(radius_, 0.0, 0.0));
            }

            double in_radius(void)const;
            double out_radius(void)const;
            double volume(void)const;
            double radius(void)const;
            const Convex* shape(void)const;

        private:
            const Convex* shape_;
            double radius_;
            double in_radius_, out_radius_;
        };

    }
}
#else
typedef struct MchShapeSphereSwept MchShapeSphereSwept;
#endif

//TODO: Implement these.
//#ifdef __cplusplus
//using MchShapeSphereSwept = MCH::shape::SphereSwept;
//extern "C" {
//#endif
//    MchShapeSphereSwept* mch_shape_make_cone(double base_radius, double height);
//    void mch_shape_delete_cone(MchShapeSphereSwept*);
//
//    //double in_radius(void)const;
//    //double out_radius(void)const;
//    //double volume(void)const;
//    //const std::vector<clam::Vec3d>& vertices(void)const;
//    //const std::vector<std::vector<unsigned int>>& faces(void)const;
//    //const clam::Vec3d& get_vertex(size_t idx)const;
//    //const std::vector<unsigned int>& get_vertex_nbs(size_t idx)const;
//
//    //clam::Vec3d support(const clam::Vec3d&)const;
//    //double max_vert_dist2(const clam::Vec3d& pos, const clam::Quatd& rot)const;
//
//#ifdef __cplusplus
//}
//#endif

#endif
