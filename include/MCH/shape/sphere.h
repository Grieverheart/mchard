#ifndef MCH_SHAPE_SPHERE_H
#define MCH_SHAPE_SPHERE_H

#ifdef __cplusplus

#include "convex.h"

namespace MCH{
    namespace shape{

        class Sphere: public Convex{
        public:
            clam::Vec3d support(const clam::Vec3d& dir)const{
                double norm = dir.length();
                return (norm > 0.0)? dir / norm: clam::Vec3d(1.0, 0.0, 0.0);
            }

            double in_radius(void)const{
                return 1.0;
            }

            double out_radius(void)const{
                return 1.0;
            }

            double radius(void)const{
                return 1.0;
            }

            double volume(void)const{
                return (4.0 * M_PI / 3.0);
            }
        };

    }
}
#else
typedef struct MchShapeSphere MchShapeSphere;
#endif

#ifdef __cplusplus
using MchShapeSphere = MCH::shape::Sphere;
extern "C" {
#endif
    MchShapeSphere* mch_shape_make_sphere(void);
    void mch_shape_delete_sphere(MchShapeSphere*);

    //double in_radius(void)const;
    //double out_radius(void)const;
    //double volume(void)const;
    //const std::vector<clam::Vec3d>& vertices(void)const;
    //const std::vector<std::vector<unsigned int>>& faces(void)const;
    //const clam::Vec3d& get_vertex(size_t idx)const;
    //const std::vector<unsigned int>& get_vertex_nbs(size_t idx)const;

    //clam::Vec3d support(const clam::Vec3d&)const;
    //double max_vert_dist2(const clam::Vec3d& pos, const clam::Quatd& rot)const;

#ifdef __cplusplus
}
#endif

#endif
