#ifndef MCH_SHAPE_VARIANT_H
#define MCH_SHAPE_VARIANT_H

#include "MCH/shape/shapes.h"
#include "MCH/shape/variant_fwd.h"

#ifdef __cplusplus
#include <boost/variant.hpp>
#endif

#ifdef __cplusplus
extern "C" {
#endif
    //TODO: Are these the way we want them to be? What about mch_config_add_shape()?
    //TODO: Add more.
    MchShapeVariant* mch_shape_polyhedron_make_variant(MchShapePolyhedron*);
    MchShapeVariant* mch_shape_sphere_make_variant(MchShapeSphere*);
    MchShapeVariant* mch_shape_cone_make_variant(MchShapeCone*);
    MchShapeVariant* mch_shape_cylinder_make_variant(MchShapeCylinder*);
    MchShapeVariant* mch_shape_box_make_variant(MchShapeBox*);
    void mch_shape_variant_delete(MchShapeVariant*);
#ifdef __cplusplus
}
#endif

//TODO: Perhaps do the following to avoid calling boost explicitly
//
//namespace MCH{
//    namespace shape{
//        template<class T>
//        using static_visitor = boost::static_visitor<T>;
//
//        template <typename... Args>
//        auto apply_visitor(Args&&... args) -> decltype(boost::apply_visitor(std::forward<Args>(args)...)){
//            return boost::apply_visitor(std::forward<Args>(args)...);
//        }
//    }
//}
//
//TODO: We can apply a visitor in plain C by defining a generic visitor which takes
//n_shapes function pointers i.e.
//namespace MCH{
//    namespace shape{
//        class ShapeGenericVisitor: public boost::static_visitor<void*>{
//        public:
//            using poly_func = void* (*)(const Polyhedron*);
//            using sph_func  = void* (*)(const Sphere*);
//
//            ShapeGenericVisitor(poly_func pf, sph_func sf):
//                pf_(pf), sf_(sf)
//            {}
//
//            void* operator()(const Polyhedron& poly)const{
//                return pf_(&poly);
//            }
//
//            void* operator()(const Sphere& sph)const{
//                return sf_(&sph);
//            }
//
//        private:
//            poly_func pf_;
//            sph_func sf_;
//        };
//    }
//}
//
//void* shape_visit(shape::Variant var, poly_func pf, sph_func sf){
//    auto visitor = ShapeGenericVisitor(pf, sf);
//    return boost::apply_visitor(visitor, var);
//}

#endif
