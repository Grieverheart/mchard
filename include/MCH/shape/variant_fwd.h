#ifndef MCH_SHAPE_VARIANT_FWD_H
#define MCH_SHAPE_VARIANT_FWD_H

//@diary: For a generic shape, we can use a boost::variant. This solution is the most
//efficient and is similar to what we would have done in functional programming.
//The problem with the variant is that users cannot implement their own shapes without
//recompiling the library.

//@diary: If we decide to interface to lua, we could add a new Convex shape,
//LuaShape: public ConvexShape, which will load a shape defined in the lua script.

#ifdef __cplusplus
#include <boost/variant/variant_fwd.hpp>

namespace MCH{
    namespace shape{

        class Polyhedron;
        class Sphere;
        class Cone;
        class Cylinder;
        class LeafCylinder;
        class Box;
        class Bicone;
        class Minkowski;
        class Hull;
        class Complex;
        class SphereSwept;

        enum eShapeTypes{
            COMPLEX      = 0,
            POLYHEDRON   = 1,
            SPHERE       = 2,
            CONE         = 3,
            CYLINDER     = 4,
            BOX          = 5,
            BICONE       = 6,
            MINKOWSKI    = 7,
            HULL         = 8,
            SPHERE_SWEPT = 9,
            N_SHAPE_TYPES
        };

        using Variant = boost::variant<Complex, Polyhedron, Sphere, Cone, Cylinder, LeafCylinder, Box, Bicone, Minkowski, Hull, SphereSwept>;
    }
}

using MchShapeVariant = MCH::shape::Variant;

#else
    typedef struct MchShapeVariant MchShapeVariant;
#endif// __cplusplus

#endif
