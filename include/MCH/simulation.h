#ifndef MC_HARD_SIMULATION_H
#define MC_HARD_SIMULATION_H

#include "configuration.h"
#include "clam/clam.h"
#ifdef __cplusplus
#include <cstdint>
#else
#include <stdint.h>
#include <stdbool.h>
#endif

typedef struct DSFMT_T dsfmt_t;

#ifdef __cplusplus
namespace MCH{

    class CellList;

    class Simulation{
    public:
        Simulation(const Configuration&);
        ~Simulation(void);

        //MC Moves
        //TODO: Add calls for axis-angle and perhaps rotation matrix.
        bool rotate_particle(size_t r_pid, const clam::Quatd& rotation);
        bool move_particle(size_t r_pid, const clam::Vec3d& dr);
        bool event_chain_move(size_t r_pid, double length, const clam::Vec3d& rdir);
        //TODO: Set pressure as argument to these calls
        bool change_volume(const clam::Vec3d& ds);
        bool cluster_change_volume(const clam::Vec3d& ds);
        bool cluster_rotate(size_t r_pid, const clam::Quatd& rotation, double cutoff_dist);

        //void sort_particles(void);

        //Getters
        const Configuration& get_configuration(void)const;
        size_t get_npart(void)const;
        double get_pressure(void)const;
        double get_volume(void)const;
        clam::Vec3d get_drift(void)const;

        //Setters
        void reseed(uint32_t seed);
        void set_pressure(double);

    private:
        Configuration config_;

        CellList* cll_;
        dsfmt_t* rng_;
        clam::Vec3d rcm_drift_;
        double pressure_;
    };
}
#else// __cplusplus

typedef struct MchSimulation MchSimulation;

#endif// __cplusplus

#ifdef __cplusplus
using MchSimulation = MCH::Simulation;
extern "C" {
#endif
    MchSimulation* mch_sim_new(const MchConfiguration* config);
    void mch_sim_delete(MchSimulation*);
    bool mch_sim_rotate_particle(MchSimulation*, size_t r_pid, double* quat);
    bool mch_sim_move_particle(MchSimulation*, size_t r_pid, double* dr);
    bool mch_sim_event_chain_move(MchSimulation*, size_t r_pid, double length, double* dir);
    bool mch_sim_change_volume(MchSimulation*, double* ds);
    bool mch_sim_cluster_change_volume(MchSimulation*, double* ds);
    //Getters
    const MchConfiguration* mch_sim_get_configuration(const MchSimulation*);
    size_t mch_sim_get_npart(const MchSimulation*);
    double mch_sim_get_pressure(const MchSimulation*);
    double mch_sim_get_volume(const MchSimulation*);
    //Setters
    void mch_sim_set_pressure(MchSimulation*, double);
#ifdef __cplusplus
}
#endif

#endif
