#include "MCH/box/Base.h"

void mch_box_base_delete(MchBoxBase* box){
    auto cpp_box = static_cast<MCH::box::Base*>(box);
    delete cpp_box;
    box = nullptr;
}

MchBoxBase* mch_box_base_clone(const MchBoxBase* box){
    return box->clone();
}

const char* mch_box_base_name(const MchBoxBase* box){
    return box->name();
}

void mch_box_base_bounds(const MchBoxBase* box, double* bounds){
    auto bounds_ = box->bounds();
    //TODO: memcpy?
    for(int i = 0; i < 3; ++i) bounds[i] = bounds_[i];
}

void mch_box_base_apply_pbc(const MchBoxBase* box, const double* pos, double* new_pos){
    auto new_pos_ = box->bounds();
    //TODO: memcpy?
    for(int i = 0; i < 3; ++i) new_pos[i] = new_pos_[i];
}

void mch_box_base_minimum_image(const MchBoxBase* box, const double* pos, double* new_pos){
    auto new_pos_ = box->bounds();
    //TODO: memcpy?
    for(int i = 0; i < 3; ++i) new_pos[i] = new_pos_[i];
}

bool mch_box_base_check_overlap(const MchBoxBase* box, const MchParticle* particle, const MchShapeVariant* shape){
    return box->check_overlap(*particle, *shape);
}

bool mch_box_base_raycast(const MchBoxBase* box, const MchParticle* particle, const MchShapeVariant* shape, const double* ray_dir, double* distance, double* normal){
    auto normal_ = clam::Vec3d();
    bool answer = box->raycast(*particle, *shape, clam::Vec3d(ray_dir), *distance, normal_);
    for(int i = 0; i < 3; ++i) normal[i] = normal_[i];
    return answer;
}

bool mch_box_base_is_hard(const MchBoxBase* box){
    return box->is_hard();
}

double mch_box_base_volume(const MchBoxBase* box){
    return box->volume();
}

void mch_box_base_rescale(MchBoxBase* box, double* factor){
    box->rescale(clam::Vec3d(factor[0], factor[1], factor[2]));
}

