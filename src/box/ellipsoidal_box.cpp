#include "MCH/box/ellipsoidal_box.h"
#include "MCH/shape/variant.h"
#include "MCH/particle.h"
#include "../overlap/ray_casting.h"

namespace MCH{

    using namespace overlap;

    namespace box{
        namespace detail{
            static const clam::Vec3d dirs[] = {
                { 1.0,  0.0,  0.0},
                {-1.0,  0.0,  0.0},
                { 0.0,  1.0,  0.0},
                { 0.0, -1.0,  0.0},
                { 0.0,  0.0,  1.0},
                { 0.0,  0.0, -1.0},

                { 1.0 / sqrt(2.0),  1.0 / sqrt(2.0), 0.0},
                {-1.0 / sqrt(2.0),  1.0 / sqrt(2.0), 0.0},
                { 1.0 / sqrt(2.0), -1.0 / sqrt(2.0), 0.0},
                {-1.0 / sqrt(2.0), -1.0 / sqrt(2.0), 0.0},

                { 1.0 / sqrt(2.0),  0.0,  1.0 / sqrt(2.0)},
                {-1.0 / sqrt(2.0),  0.0,  1.0 / sqrt(2.0)},
                { 1.0 / sqrt(2.0),  0.0, -1.0 / sqrt(2.0)},
                {-1.0 / sqrt(2.0),  0.0, -1.0 / sqrt(2.0)},

                { 0.0,  1.0 / sqrt(2.0),  1.0 / sqrt(2.0)},
                { 0.0, -1.0 / sqrt(2.0),  1.0 / sqrt(2.0)},
                { 0.0,  1.0 / sqrt(2.0), -1.0 / sqrt(2.0)},
                { 0.0, -1.0 / sqrt(2.0), -1.0 / sqrt(2.0)},

                { 1.0 / sqrt(3.0),  1.0 / sqrt(3.0),  1.0 / sqrt(3.0)},
                {-1.0 / sqrt(3.0), -1.0 / sqrt(3.0), -1.0 / sqrt(3.0)},
                { 1.0 / sqrt(3.0),  1.0 / sqrt(3.0), -1.0 / sqrt(3.0)},
                {-1.0 / sqrt(3.0), -1.0 / sqrt(3.0),  1.0 / sqrt(3.0)},
                { 1.0 / sqrt(3.0), -1.0 / sqrt(3.0),  1.0 / sqrt(3.0)},
                {-1.0 / sqrt(3.0),  1.0 / sqrt(3.0), -1.0 / sqrt(3.0)},
                {-1.0 / sqrt(3.0),  1.0 / sqrt(3.0),  1.0 / sqrt(3.0)},
                { 1.0 / sqrt(3.0), -1.0 / sqrt(3.0), -1.0 / sqrt(3.0)},
            };
        }

        EllipsoidalBox::EllipsoidalBox(const clam::Vec3d& radius):
            radius_(radius)
        {}

        Base* EllipsoidalBox::clone(void)const{
            return new EllipsoidalBox(radius_);
        }

        const char* EllipsoidalBox::name(void)const{
            return "Ellipsoidal";
        }

        clam::Vec3d EllipsoidalBox::bounds(void)const{
            return 2.0 * radius_;
        }

        void EllipsoidalBox::rescale(const clam::Vec3d& factor){
            radius_ *= factor;
        }

        bool EllipsoidalBox::is_hard(void)const{
            return true;
        }

        double EllipsoidalBox::volume(void)const{
            return (4.0 * M_PI * radius_[0] * radius_[1] * radius_[2])/ 3.0;
        }

        clam::Vec3d EllipsoidalBox::apply_pbc(const clam::Vec3d& pos)const{
            return pos;
        }

        clam::Vec3d EllipsoidalBox::minimum_image(const clam::Vec3d& dist)const{
            return dist;
        }

        clam::Vec3d EllipsoidalBox::minimum_image_vector(const clam::Vec3d& dist)const{
            return clam::Vec3d(0.0);
        }

        class EllipsoidalBox::ShapeOverlapVisitor: public boost::static_visitor<bool>{
        public:
            ShapeOverlapVisitor(const EllipsoidalBox& box, const Particle& particle):
                box_(box), p(particle)
            {}

            bool operator()(const shape::Polyhedron& shape)const{
                auto cpos = p.pos - box_.radius_;
                //Check if polyhedron is definitely on or outside the ellipsoid by using the polyhedorn in-radius
                if((cpos / (box_.radius_ - p.size * shape.in_radius())).length2() >= 1.0) return true;
                //Check if polyhedron is completely inside the ellipsoid by using the polyhedorn out-radius
                else if((cpos / (box_.radius_ - p.size * shape.out_radius())).length2() < 1.0) return false;
                //Check if any vertex is outside the ellipsoid
                else{
                    for(auto vertex: shape.vertices()){
                        if(((p.size * p.rot.rotate(vertex) + p.pos - box_.radius_) / box_.radius_).length2() >= 1.0) return true;
                    }
                    return false;
                }
            }

            bool operator()(const shape::Sphere& sph)const{
                auto cpos = (p.pos - clam::Vec3d(box_.radius_)) / (box_.radius_ - p.size * sph.radius());
                return cpos.length2() > 1.0;
            }

            bool operator()(const shape::Complex& comp)const{
                const shape::ConvexShapeDefinition* shape_defs = comp.shape_definitions();
                Particle pp;
                for(size_t sdid = 0; sdid < comp.num_shape_definitions(); ++sdid){
                    const shape::ConvexShapeDefinition& sdef = shape_defs[sdid];
                    pp.pos  = p.pos + p.size * p.rot.rotate(sdef.pos_);
                    pp.rot  = sdef.rot_ * p.rot;
                    pp.size = sdef.size_ * p.size;
                    if((ShapeOverlapVisitor(box_, pp))(*sdef.shape_)) return true;
                }
                return false;
            }

            //NOTE: Only works for spherical confinement.
            //bool operator()(const shape::Cylinder& cyl)const{
            //    double L = p.size * cyl.base_radius();
            //    clam::Quatd inv_rot = p.rot.inv();
            //    clam::Vec3d pos = inv_rot.rotate(p.pos - box_.radius_);
            //    clam::Vec3d c = pos + clam::Vec3d(0.0, std::copysign(0.5 * cyl.height(), pos[1]), 0.0);
            //    clam::Vec3d add = clam::Vec3d(pos[0], 0.0, pos[2]);
            //    clam::Vec3d point = c + add * (L / add.length());
            //    if((point / box_.radius_).length2() >= 1.0) return true;

            //    return false;
            //}

            //NOTE: Most probably, there might be some cases that are not covered by these directions.
            template<class T>
            bool operator()(const T& shape)const{
                auto cpos = (p.pos - clam::Vec3d(box_.radius_)) / (box_.radius_ - p.size * shape.out_radius());
                if(cpos.length2() < 1.0) return false;

                cpos = p.pos - box_.radius_;
                for(auto dir: detail::dirs){
                    if(clam::dot(cpos, p.rot.rotate(dir)) < 0.0) continue;
                    //TODO: Add a map storing the support points for each shape, with the shape pointer acting as the key.
                    auto point = p.pos + p.rot.rotate(p.size * shape.support(dir));
                    if(((point - box_.radius_) / box_.radius_).length2() >= 1.0) return true;
                }

                return false;
            }
        private:
            const EllipsoidalBox& box_;
            const Particle& p;
        };

        class EllipsoidalBox::ShapeRaycastVisitor: public boost::static_visitor<bool>{
        public:
            ShapeRaycastVisitor(const EllipsoidalBox& box, const Particle& particle, const clam::Vec3d& ray_dir, double& distance, clam::Vec3d& normal):
                box_(box), p(particle), ray_dir_(ray_dir), normal_(normal), dist_(distance)
            {}

            bool operator()(const shape::Polyhedron& shape)const{
                bool found = false;
                for(auto vertex: shape.vertices()){
                    double t = 0.0;
                    clam::Vec3d normal;
                    if(ellipsoid_raycast(box_.radius_, box_.radius_ - p.pos - p.size * p.rot.rotate(vertex), ray_dir_, t, normal) && t < dist_){
                        dist_ = t;
                        normal_ = normal;
                        found = true;
                    }
                }
                //Particle will always hit a wall since it's inside
                return found;
            }

            bool operator()(const shape::Sphere& sph)const{
                return ellipsoid_raycast(box_.radius_ - p.size * sph.radius(), box_.radius_ - p.pos, ray_dir_, dist_, normal_);
            }

            //TODO: Implement this?
            bool operator()(const shape::Complex& comp)const{
                assert(false);
                return true;
            }

            template<class T>
            bool operator()(const T& shape)const{
                bool found = false;
                for(auto dir: detail::dirs){
                    //TODO: We should be able to skip back-facing vertices.
                    auto point = p.size * p.rot.rotate(shape.support(dir));
                    double t = 0.0;
                    clam::Vec3d normal;
                    if(ellipsoid_raycast(box_.radius_, box_.radius_ - p.pos - point, ray_dir_, t, normal) && t < dist_){
                        dist_ = t;
                        normal_ = normal;
                        found = true;
                    }
                }

                return found;
            }
        private:
            const EllipsoidalBox& box_;
            const Particle& p;
            const clam::Vec3d& ray_dir_;
            clam::Vec3d& normal_;
            double& dist_;
        };

        bool EllipsoidalBox::check_overlap(const Particle& p, const shape::Variant& shape)const{
            return boost::apply_visitor(ShapeOverlapVisitor(*this, p), shape);
        }

        bool EllipsoidalBox::raycast(const Particle& p, const shape::Variant& shape, const clam::Vec3d& ray_dir, double& distance, clam::Vec3d& normal)const{
            return boost::apply_visitor(ShapeRaycastVisitor(*this, p, ray_dir, distance, normal), shape);
        }

    }
}

MchBoxBase* mch_box_make_spherical_box(double* radius){
    return new MCH::box::EllipsoidalBox(clam::Vec3d(radius[0], radius[1], radius[2]));
}

