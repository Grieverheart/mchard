#include "MCH/box/rectangular_box.h"
#include "MCH/shape/variant.h"
#include "MCH/particle.h"
#include "../overlap/ray_casting.h"

namespace MCH{
    using namespace overlap;
    namespace box{

        RectangularBox::RectangularBox(const clam::Vec3d& size, bool is_hard):
            size_(size), is_hard_(is_hard)
        {}

        Base* RectangularBox::clone(void)const{
            return new RectangularBox(size_, is_hard_);
        }

        const char* RectangularBox::name(void)const{
            return "Rectangular";
        }

        clam::Vec3d RectangularBox::bounds(void)const{
            return size_;
        }

        void RectangularBox::rescale(const clam::Vec3d& factor){
            size_ *= factor;
        }

        bool RectangularBox::is_hard(void)const{
            return is_hard_;
        }

        double RectangularBox::volume(void)const{
            return size_[0] * size_[1] * size_[2];
        }

        clam::Vec3d RectangularBox::apply_pbc(const clam::Vec3d& pos)const{
            clam::Vec3d ret = pos;
            for(int i = 0; i < 3; ++i){
                ret[i] += (ret[i] >= size_[i])? -size_[i]: (ret[i] < 0.0)? size_[i]: 0.0;
            }
            return ret;
        }

        clam::Vec3d RectangularBox::minimum_image(const clam::Vec3d& dist)const{
            return dist + minimum_image_vector(dist);
        }

        clam::Vec3d RectangularBox::minimum_image_vector(const clam::Vec3d& dist)const{
            clam::Vec3d ret;
            for(int i = 0; i < 3; ++i){
                ret[i] = (dist[i] > 0.5 * size_[i])? -size_[i]: (dist[i] <  -0.5 * size_[i])?  size_[i]: 0.0;
            }
            return ret;
        }

        class RectangularBox::ShapeOverlapVisitor: public boost::static_visitor<bool>{
        public:
            ShapeOverlapVisitor(const RectangularBox& box, const Particle& particle):
                box_(box), p(particle)
            {}

            template<class T>
            bool operator()(const T& shape)const{
                //For each x, y, z, check if point is outside the corresponding planes
                for(int i = 0; i < 3; ++i){
                    clam::Vec3d dir(0.0);

                    //TODO: Not sure if this is really needed
                    if((p.pos[i] >= box_.size_[i] - p.size * shape.in_radius()) ||
                       (p.pos[i] < p.size * shape.in_radius())
                    ) return true;

                    if(p.pos[i] >= box_.size_[i] - p.size * shape.out_radius()){
                        dir[i] = 1.0;
                        dir = p.rot.inv().rotate(dir);
                        //We find the point farthest in the plane direction
                        clam::Vec3d point = p.pos + p.size * p.rot.rotate(shape.support(dir));
                        if(point[i] >= box_.size_[i]) return true;
                    }
                    else if(p.pos[i] < p.size * shape.out_radius()){
                        dir[i] = -1.0;
                        dir = p.rot.inv().rotate(dir);
                        //We find the point farthest in the plane direction
                        clam::Vec3d point = p.pos + p.size * p.rot.rotate(shape.support(dir));
                        if(point[i] < 0.0) return true;
                    }
                }
                return false;
            }

            bool operator()(const shape::Sphere& sph)const{
                for(int i = 0; i < 3; ++i){
                    if((p.pos[i] > box_.size_[i] - p.size * sph.radius()) ||
                       (p.pos[i] < p.size * sph.radius())) return true;
                }
                return false;
            }

            bool operator()(const shape::Complex& comp)const{
                const shape::ConvexShapeDefinition* shape_defs = comp.shape_definitions();
                Particle pp;
                for(size_t sdid = 0; sdid < comp.num_shape_definitions(); ++sdid){
                    const shape::ConvexShapeDefinition& sdef = shape_defs[sdid];
                    pp.pos  = p.pos + p.size * p.rot.rotate(sdef.pos_);
                    pp.rot  = sdef.rot_ * p.rot;
                    pp.size = sdef.size_ * p.size;
                    if((ShapeOverlapVisitor(box_, pp))(*sdef.shape_)) return true;
                }
                return false;
            }

        private:
            const RectangularBox& box_;
            const Particle& p;
        };

        //NOTE: In principle this should never return false.
        class RectangularBox::ShapeRaycastVisitor: public boost::static_visitor<bool>{
        public:
            ShapeRaycastVisitor(const RectangularBox& box, const Particle& particle, const clam::Vec3d& ray_dir, double& distance, clam::Vec3d& normal):
                box_(box), p(particle), ray_dir_(ray_dir), normal_(normal), dist_(distance)
            {}

            bool operator()(const shape::Polyhedron& shape)const{
                bool found = false;
                for(auto vertex: shape.vertices()){
                    double t = 0.0;
                    clam::Vec3d normal;
                    if(AABB_raycast(clam::Vec3d(0.0), box_.size_, p.pos + p.size * p.rot.rotate(vertex), ray_dir_, t, normal) && t < dist_){
                        dist_ = t;
                        normal_ = normal;
                        found = true;
                    }
                }
                //Particle will always hit a wall since it's inside
                return found;
            }

            bool operator()(const shape::Sphere& sph)const{
                return AABB_raycast(clam::Vec3d(0.0), box_.size_ - p.size * sph.radius(), p.pos, ray_dir_, dist_, normal_);
            }

            bool operator()(const shape::Complex& comp)const{
                const shape::ConvexShapeDefinition* shape_defs = comp.shape_definitions();
                Particle pp;
                for(size_t sdid = 0; sdid < comp.num_shape_definitions(); ++sdid){
                    const shape::ConvexShapeDefinition& sdef = shape_defs[sdid];
                    pp.pos  = p.pos + p.size * p.rot.rotate(sdef.pos_);
                    pp.rot  = sdef.rot_ * p.rot;
                    pp.size = sdef.size_ * p.size;
                    if((ShapeRaycastVisitor(box_, pp, ray_dir_, dist_, normal_))(*sdef.shape_)) return true;
                }
                return false;
            }

            //TODO: Also test performance against polyhedron algorithm.
            //This one might be faster since only a maximum of 4 raycasts
            //are performed.
            template<class T>
            bool operator()(const T& shape)const{
                auto inv_rot = p.rot.inv();
                std::vector<clam::Vec3d> points;
                points.push_back(p.pos + p.size * p.rot.rotate(shape.support(inv_rot.rotate(ray_dir_))));
                for(int i = 0; i < 3; ++i){
                    if(ray_dir_[i] != 0.0){
                        clam::Vec3d dir(0.0);
                        dir[i] = std::copysign(1.0, ray_dir_[i]);
                        points.push_back(p.pos + p.size * p.rot.rotate(shape.support(inv_rot.rotate(dir))));
                    }
                }

                bool found = false;
                for(auto point: points){
                    double t = 0.0;
                    clam::Vec3d normal;
                    if(AABB_raycast(clam::Vec3d(0.0), box_.size_, point, ray_dir_, t, normal) && t < dist_){
                        dist_ = t;
                        normal_ = normal;
                        found = true;
                    }
                }
                //Particle will always hit a wall since it's inside
                return found;
            }
        private:
            const RectangularBox& box_;
            const Particle& p;
            const clam::Vec3d& ray_dir_;
            clam::Vec3d& normal_;
            double& dist_;
        };

        bool RectangularBox::check_overlap(const Particle& p, const shape::Variant& shape)const{
            return boost::apply_visitor(ShapeOverlapVisitor(*this, p), shape);
        }

        bool RectangularBox::raycast(const Particle& p, const shape::Variant& shape, const clam::Vec3d& ray_dir, double& distance, clam::Vec3d& normal)const{
            return boost::apply_visitor(ShapeRaycastVisitor(*this, p, ray_dir, distance, normal), shape);
        }

    }
}

MchBoxBase* mch_box_make_rectangular_box(double* size){
    return new MCH::box::RectangularBox(clam::Vec3d(size[0], size[1], size[2]));
}
