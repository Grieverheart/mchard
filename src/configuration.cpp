#include "MCH/configuration.h"
#include "MCH/particle.h"
#include "MCH/box/Base.h"
#include "MCH/shape/variant.h"

namespace MCH{

Configuration::Configuration(void):
    n_part_(0),
    particles_(nullptr), box_(nullptr)
{}

Configuration::~Configuration(void){
    delete[] particles_;
    delete box_;
}

Configuration::Configuration(
    const box::Base* box,
    int n_part, const Particle* particles
):
    n_part_(n_part),
    particles_(new Particle[n_part]),
    box_(box->clone())
{
    for(size_t i = 0; i < n_part_; ++i) particles_[i] = particles[i];
}

//TODO: Unsafe when config is not initialized
Configuration::Configuration(const Configuration& config):
    n_part_(config.n_part_),
    particles_(n_part_? new Particle[n_part_]: nullptr),
    box_(config.box_? config.box_->clone(): nullptr)
{
    for(size_t i = 0; i < n_part_; ++i) particles_[i] = config.particles_[i];
}

}

MchConfiguration* mch_config_new(void){
    return static_cast<MchConfiguration*>(new MCH::Configuration());
}

void mch_config_delete(MchConfiguration* config){
    auto cpp_config = static_cast<MCH::Configuration*>(config);
    delete cpp_config;
    config = nullptr;
}
