#ifndef DSFMT_RNG_H
#define DSFMT_RNG_H

#define DSFMT_MEXP 19937
#define HAVE_SSE2
#include "dSFMT.h"

class DSFMTRng{
public:
    explicit DSFMTRng(uint32_t seed):
        seed_(seed)
    {
        dsfmt_init_gen_rand(&rng_, seed);
    }

    DSFMTRng(void):
        seed_(0)
    {
        dsfmt_init_gen_rand(&rng_, 0u);
    }

    void seed(uint32_t seed){
        seed_ = seed;
        dsfmt_init_gen_rand(&rng_, seed);
    }

    uint32_t get_seed(void)const{
        return seed_;
    }

    double next_f64(void){
        dsfmt_genrand_close_open(&rng_);
    }

    uint32_t next_u32(void){
        dsfmt_genrand_uint32(&rng_);
    }

    double range_f64(double low, double high);
    uint32_t range_u32(uint32_t low, uint32_t high);
private:
    dsfmt_t rng_;
    uint32_t seed_;
};

#endif
