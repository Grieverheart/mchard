#include "MCH/io/config_xml.h"
#include "MCH/shape/variant.h"
#include "MCH/particle.h"
#include "MCH/configuration.h"
#include "MCH/box/boxes.h"
#include "obj_loader.h"
#include "tinyxml2.h"
#include <unistd.h>
#include <unordered_map>
#include <boost/variant/polymorphic_get.hpp>

//TODO: Handle errors.

namespace MCH{
    namespace io{

        void print_convex_shape_def(FILE* fp, const shape::ConvexShapeDefinition& shape_def, const shape::Variant** shapes, size_t n_shapes){
            fprintf(fp, "<shape_def>\n");

            fprintf(fp, "<pos>\n");
            fprintf(fp, "<x>%f</x><y>%f</y><z>%f</z>\n", shape_def.pos_[0], shape_def.pos_[1], shape_def.pos_[2]);
            fprintf(fp, "</pos>\n");

            //TODO: Perhaps skip this if the particle shape is a sphere?
            fprintf(fp, "<quat>\n");
            fprintf(fp, "<x>%f</x><y>%f</y><z>%f</z><w>%f</w>\n", shape_def.rot_[0], shape_def.rot_[1], shape_def.rot_[2], shape_def.rot_[3]);
            fprintf(fp, "</quat>\n");

            if(shape_def.size_ != 1.0){
                fprintf(fp, "<size>%f</size>\n", shape_def.size_);
            }

            if(n_shapes > 1){
                const shape::Convex* shape = shape_def.shape_;
                bool sid_found = false;
                for(size_t sid = 0; sid < n_shapes; ++sid){
                    if(shape == boost::polymorphic_get<shape::Convex>(shapes[sid])){
                        fprintf(fp, "<shape id=\"%lu\"/>\n", sid);
                        sid_found = true;
                        break;
                    }
                }
                if(!sid_found) printf("Error finding shape id for Complex.\n");
            }

            fprintf(fp, "</shape_def>\n");
        }

        class ShapePrintVisitor: public boost::static_visitor<>{
        public:
            ShapePrintVisitor(FILE* fp, const shape::Variant** shapes, size_t n_shapes):
                fp_(fp),
                shapes_(shapes), n_shapes_(n_shapes)
            {}

            void operator()(const shape::Polyhedron& poly)const{
                const char* source = poly.get_source();
                if(source){
                    fprintf(fp_, "<shape type=\"polyhedron\" src=\"%s\"/>\n", source);
                }
                else{
                    fprintf(fp_, "<shape type=\"polyhedron\">\n<vertices>\n");
                    for(auto vert: poly.vertices()){
                        fprintf(fp_, "<vertex><x>%f</x><y>%f</y><z>%f</z></vertex>\n", vert[0], vert[1], vert[2]);
                    }
                    fprintf(fp_, "</vertices>\n<faces>\n");
                    for(auto face: poly.faces()){
                        fprintf(fp_, "<face>\n");
                        for(auto face_idx: face){
                            fprintf(fp_, "<fi>%u</fi>\n", face_idx);
                        }
                        fprintf(fp_, "</face>\n");
                    }
                    fprintf(fp_, "</faces>\n</shape>\n");
                }
            }

            void operator()(const shape::Sphere& sph)const{
                fprintf(fp_, "<shape type=\"sphere\">\n");
                fprintf(fp_, "<radius>%f</radius>", sph.radius());
                fprintf(fp_, "</shape>\n");
            }

            void operator()(const shape::Cone& cone)const{
                fprintf(fp_, "<shape type=\"cone\">\n");
                fprintf(fp_, "<base_radius>%f</base_radius>\n<height>%f</height>\n", cone.base_radius(), cone.height());
                fprintf(fp_, "</shape>\n");
            }

            void operator()(const shape::Bicone& bicone)const{
                fprintf(fp_, "<shape type=\"bicone\">\n");
                fprintf(fp_, "<base_radius>%f</base_radius>\n<height>%f</height>\n", bicone.base_radius(), bicone.height());
                fprintf(fp_, "</shape>\n");
            }

            void operator()(const shape::Cylinder& cyl)const{
                fprintf(fp_, "<shape type=\"cylinder\">\n");
                fprintf(fp_, "<base_radius>%f</base_radius>\n<height>%f</height>\n", cyl.base_radius(), cyl.height());
                fprintf(fp_, "</shape>\n");
            }

            void operator()(const shape::LeafCylinder& leaf)const{
                fprintf(fp_, "<shape type=\"leaf cylinder\">\n");
                fprintf(fp_, "<width>%f</width>\n<length>%f</length>\n<height>%f</height>\n", leaf.width(), leaf.length(), leaf.height());
                fprintf(fp_, "</shape>\n");
            }

            void operator()(const shape::Box& box)const{
                fprintf(fp_, "<shape type=\"box\">\n");
                fprintf(fp_, "<x>%f</x><y>%f</y><z>%f</z>\n", 2.0 * box.extent()[0], 2.0 * box.extent()[1], 2.0 * box.extent()[2]);
                fprintf(fp_, "</shape>\n");
            }

            void operator()(const shape::Minkowski& mink)const{
                const shape::ConvexShapeDefinition& pa = mink.shape_def_a();
                const shape::ConvexShapeDefinition& pb = mink.shape_def_b();
                fprintf(fp_, "<shape type=\"minkowski\">\n");
                print_convex_shape_def(fp_, pa, shapes_, n_shapes_);
                print_convex_shape_def(fp_, pb, shapes_, n_shapes_);
                fprintf(fp_, "</shape>\n");
            }

            void operator()(const shape::SphereSwept& ss)const{
                fprintf(fp_, "<shape type=\"sphere-swept\">\n");
                fprintf(fp_, "<radius>%f</radius>\n", ss.radius());

                if(n_shapes_ > 1){
                    const shape::Convex* shape = ss.shape();
                    bool sid_found = false;
                    for(size_t sid = 0; sid < n_shapes_; ++sid){
                        if(shape == boost::polymorphic_get<shape::Convex>(shapes_[sid])){
                            fprintf(fp_, "<shape id=\"%lu\"/>\n", sid);
                            sid_found = true;
                            break;
                        }
                    }
                    if(!sid_found) printf("Error finding shape id for Complex.\n");
                }

                fprintf(fp_, "</shape>\n");
            }

            void operator()(const shape::Hull& hull)const{
                const shape::ConvexShapeDefinition* shape_defs = hull.shape_definitions();
                size_t n_shape_defs = hull.num_shape_definitions();
                fprintf(fp_, "<shape type=\"hull\">\n");
                for(size_t sdid = 0; sdid < n_shape_defs; ++sdid){
                    print_convex_shape_def(fp_, shape_defs[sdid], shapes_, n_shapes_);
                }
                fprintf(fp_, "</shape>\n");
            }

            void operator()(const shape::Complex& comp)const{
                const shape::ConvexShapeDefinition* shape_defs = comp.shape_definitions();
                size_t n_shape_defs = comp.num_shape_definitions();
                fprintf(fp_, "<shape type=\"complex\">\n");
                for(size_t sdid = 0; sdid < n_shape_defs; ++sdid){
                    print_convex_shape_def(fp_, shape_defs[sdid], shapes_, n_shapes_);
                }
                fprintf(fp_, "</shape>\n");
            }
        private:
            FILE* fp_;
            const shape::Variant** shapes_;
            size_t n_shapes_;
        };

        bool xml_save_config(const char* filename, const Configuration& config, const shape::Variant** shapes, size_t n_shapes){
            FILE* fp = fopen(filename, "w");
            if(!fp) return false;

            fprintf(fp, "<sim id=\"%d\">\n", getpid());{
                fprintf(fp, "<definitions>\n");{
                    fprintf(fp, "<box shape=\"%s\" pbc=\"%s\">\n", config.box_->name(), (config.box_->is_hard())? "false": "true");{
                        if(strcmp(config.box_->name(), "Rectangular") == 0){
                            const clam::Vec3d& size = static_cast<const box::RectangularBox*>(config.box_)->size();
                            fprintf(fp, "<size>\n");
                            fprintf(fp, "<x>%f</x>\n", size[0]);
                            fprintf(fp, "<y>%f</y>\n", size[1]);
                            fprintf(fp, "<z>%f</z>\n", size[2]);
                            fprintf(fp, "</size>\n");
                        }
                        else{
                            const clam::Vec3d& radius = static_cast<const box::EllipsoidalBox*>(config.box_)->radius();
                            fprintf(fp, "<radius>\n");
                            fprintf(fp, "<x>%f</x>\n", radius[0]);
                            fprintf(fp, "<y>%f</y>\n", radius[1]);
                            fprintf(fp, "<z>%f</z>\n", radius[2]);
                            fprintf(fp, "</radius>\n");
                        }
                    }fprintf(fp, "</box>\n");
                    fprintf(fp, "<shapes>\n");{
                        for(size_t sh = 0; sh < n_shapes; ++sh){
                            boost::apply_visitor(ShapePrintVisitor(fp, shapes, n_shapes), *shapes[sh]);
                        }
                    }fprintf(fp, "</shapes>\n");
                }fprintf(fp, "</definitions>\n");

                //TODO: Use pointer offset instead
                std::unordered_map<const shape::Variant*, size_t> shape_ids;
                for(size_t sid = 0; sid < n_shapes; ++sid){
                    shape_ids[shapes[sid]] = sid;
                }

                fprintf(fp, "<particles>\n");{
                    for(size_t i = 0; i < config.n_part_; ++i){
                        const auto& particle = config.particles_[i];
                        fprintf(fp, "<particle>\n");

                        fprintf(fp, "<pos>\n");
                        fprintf(fp, "<x>%f</x><y>%f</y><z>%f</z>\n", particle.pos[0], particle.pos[1], particle.pos[2]);
                        fprintf(fp, "</pos>\n");

                        //TODO: Perhaps skip this if the particle shape is a sphere?
                        fprintf(fp, "<quat>\n");
                        fprintf(fp, "<x>%f</x><y>%f</y><z>%f</z><w>%f</w>\n", particle.rot[0], particle.rot[1], particle.rot[2], particle.rot[3]);
                        fprintf(fp, "</quat>\n");

                        if(particle.size != 1.0){
                            fprintf(fp, "<size>%f</size>\n", particle.size);
                        }

                        if(n_shapes) fprintf(fp, "<shape id=\"%lu\"/>\n", shape_ids.at(particle.shape));

                        fprintf(fp, "</particle>\n");
                    }
                }fprintf(fp, "</particles>\n");
            }fprintf(fp, "</sim>");

            fclose(fp);

            return true;
        }

        bool xml_load_config(const char* filename, Configuration& config, const shape::Variant*** shapes_, size_t* n_shapes_){
            using namespace tinyxml2;
            XMLDocument doc;
            auto error = doc.LoadFile(filename);
            if(error != XML_NO_ERROR){
                printf("Error loading xml.\n");
                doc.PrintError();
                return false;
            }

            XMLElement* root = doc.RootElement();
            if(!root) return false;

            const XMLElement* definitions = root->FirstChildElement("definitions");
            if(!definitions) return false;
            //Load box
            {
                const XMLElement* box = definitions->FirstChildElement("box");
                if(!box) return false;

                bool box_has_pbc = box->BoolAttribute("pbc");

                if(box->Attribute("shape", "Rectangular")){
                    clam::Vec3d box_size(1.0);
                    const XMLElement* size_node = box->FirstChildElement("size");
                    size_node->FirstChildElement("x")->QueryDoubleText(&box_size[0]);
                    size_node->FirstChildElement("y")->QueryDoubleText(&box_size[1]);
                    size_node->FirstChildElement("z")->QueryDoubleText(&box_size[2]);
                    config.box_ = new box::RectangularBox(box_size, !box_has_pbc);
                }
                else if(box->Attribute("shape", "Ellipsoidal")){
                    clam::Vec3d box_radius(1.0);
                    const XMLElement* size_node = box->FirstChildElement("radius");
                    size_node->FirstChildElement("x")->QueryDoubleText(&box_radius[0]);
                    size_node->FirstChildElement("y")->QueryDoubleText(&box_radius[1]);
                    size_node->FirstChildElement("z")->QueryDoubleText(&box_radius[2]);
                    config.box_ = new box::EllipsoidalBox(box_radius);
                }
                //NOTE: Support for reading old box formats
                else if(box->Attribute("shape", "Cubic")){
                    double box_size = 1.0;
                    box->FirstChildElement("size")->QueryDoubleText(&box_size);
                    config.box_ = new box::RectangularBox(clam::Vec3d(box_size), !box_has_pbc);
                }
                else if(box->Attribute("shape", "Spherical")){
                    double box_radius = 1.0;
                    box->FirstChildElement("radius")->QueryDoubleText(&box_radius);
                    config.box_ = new box::EllipsoidalBox(clam::Vec3d(box_radius));
                }
                else{
                    printf("Unknown box type: %s.\n", box->Attribute("shape"));
                    return false;
                }
            }
            //Load shapes
            std::vector<const shape::Variant*> vshapes;
            {
                const XMLElement* shapes = definitions->FirstChildElement("shapes");
                if(!shapes) return false;

                for(const XMLElement* shape = shapes->FirstChildElement(); shape != nullptr; shape = shape->NextSiblingElement()){
                    if(shape->Attribute("type", "polyhedron")){
                        std::vector<clam::Vec3d> vertices;
                        std::vector<std::vector<unsigned int>> faces;

                        if(const char* source = shape->Attribute("src")){
                            if(!load_obj(source, vertices, faces)) return false;
                        }
                        else{
                            const XMLElement* v_node = shape->FirstChildElement("vertices");
                            if(!v_node) return false;

                            for(const XMLElement* vert = v_node->FirstChildElement();
                                vert != nullptr;
                                vert = vert->NextSiblingElement()
                            ){
                                clam::Vec3d vertex;
                                vert->FirstChildElement("x")->QueryDoubleText(&vertex[0]);
                                vert->FirstChildElement("y")->QueryDoubleText(&vertex[1]);
                                vert->FirstChildElement("z")->QueryDoubleText(&vertex[2]);
                                vertices.push_back(vertex);
                            }

                            const XMLElement* f_node = shape->FirstChildElement("faces");
                            if(!f_node) return false;

                            //Loop over faces
                            for(const XMLElement* fi_node = f_node->FirstChildElement();
                                fi_node != nullptr;
                                fi_node = fi_node->NextSiblingElement()
                            ){
                                std::vector<unsigned int> face;
                                //Loop over fi
                                for(const XMLElement* fi = fi_node->FirstChildElement(); fi != nullptr; fi = fi->NextSiblingElement()){
                                    unsigned int f_idx = 0;
                                    fi->QueryUnsignedText(&f_idx);
                                    face.push_back(f_idx);
                                }
                                faces.push_back(face);
                            }
                        }

                        vshapes.push_back(new shape::Variant(shape::Polyhedron(vertices, faces)));
                    }
                    else if(shape->Attribute("type", "sphere")){
                        //TODO: Should we handle radius? Maybe not, since particle already has a size attribute.
                        vshapes.push_back(new shape::Variant(shape::Sphere()));
                    }
                    else if(shape->Attribute("type", "cone")){
                        double base_radius, height;
                        shape->FirstChildElement("base_radius")->QueryDoubleText(&base_radius);
                        shape->FirstChildElement("height")->QueryDoubleText(&height);
                        vshapes.push_back(new shape::Variant(shape::Cone(base_radius, height)));
                    }
                    else if(shape->Attribute("type", "bicone")){
                        double base_radius, height;
                        shape->FirstChildElement("base_radius")->QueryDoubleText(&base_radius);
                        shape->FirstChildElement("height")->QueryDoubleText(&height);
                        vshapes.push_back(new shape::Variant(shape::Bicone(base_radius, height)));
                    }
                    else if(shape->Attribute("type", "cylinder")){
                        double base_radius, height;
                        shape->FirstChildElement("base_radius")->QueryDoubleText(&base_radius);
                        shape->FirstChildElement("height")->QueryDoubleText(&height);
                        vshapes.push_back(new shape::Variant(shape::Cylinder(base_radius, height)));
                    }
                    else if(shape->Attribute("type", "leaf cylinder")){
                        double width, length, height;
                        shape->FirstChildElement("width")->QueryDoubleText(&width);
                        shape->FirstChildElement("length")->QueryDoubleText(&length);
                        shape->FirstChildElement("height")->QueryDoubleText(&height);
                        vshapes.push_back(new shape::Variant(shape::LeafCylinder(width, length, height)));
                    }
                    else if(shape->Attribute("type", "box")){
                        clam::Vec3d size(0.0);
                        shape->FirstChildElement("x")->QueryDoubleText(&size[0]);
                        shape->FirstChildElement("y")->QueryDoubleText(&size[1]);
                        shape->FirstChildElement("z")->QueryDoubleText(&size[2]);
                        vshapes.push_back(new shape::Variant(shape::Box(size)));
                    }
                    else if(shape->Attribute("type", "sphere-swept")){
                        double radius;
                        shape->FirstChildElement("radius")->QueryDoubleText(&radius);
                        size_t shape_id = 0;
                        const XMLElement* shape_element = shape->FirstChildElement("shape");
                        if(shape_element){
                            shape_id = shape_element->UnsignedAttribute("id");
                            if(shape_id >= vshapes.size()){
                                printf("Error reading convex shape. Shape at provided id not found.\n");
                                return false;
                            }
                        }
                        vshapes.push_back(new shape::Variant(shape::SphereSwept(boost::polymorphic_get<shape::Convex>(vshapes[shape_id]), radius)));
                    }
                    else if(shape->Attribute("type", "minkowski")){
                        std::vector<shape::ConvexShapeDefinition> shape_defs;
                        for(const XMLElement* shape_def_node = shape->FirstChildElement("shape_def");
                            shape_def_node != nullptr;
                            shape_def_node = shape_def_node->NextSiblingElement()
                        ){
                            shape::ConvexShapeDefinition def;
                            double x, y, z, w;

                            const XMLElement* pos_element = shape_def_node->FirstChildElement("pos");
                            pos_element->FirstChildElement("x")->QueryDoubleText(&x);
                            pos_element->FirstChildElement("y")->QueryDoubleText(&y);
                            pos_element->FirstChildElement("z")->QueryDoubleText(&z);
                            def.pos_ = clam::Vec3d(x, y, z);

                            const XMLElement* rot_element = shape_def_node->FirstChildElement("quat");
                            rot_element->FirstChildElement("x")->QueryDoubleText(&x);
                            rot_element->FirstChildElement("y")->QueryDoubleText(&y);
                            rot_element->FirstChildElement("z")->QueryDoubleText(&z);
                            rot_element->FirstChildElement("w")->QueryDoubleText(&w);
                            def.rot_ = clam::Quatd(x, y, z, w);

                            const XMLElement* size_element = shape_def_node->FirstChildElement("size");
                            if(size_element) size_element->QueryDoubleText(&def.size_);
                            else def.size_ = 1.0;

                            size_t shape_id = 0;
                            const XMLElement* shape_element = shape_def_node->FirstChildElement("shape");
                            if(shape_element){
                                shape_id = shape_element->UnsignedAttribute("id");
                                if(shape_id >= vshapes.size()){
                                    printf("Error reading convex shape. Shape at provided id not found.\n");
                                    return false;
                                }
                            }
                            def.shape_ = boost::polymorphic_get<shape::Convex>(vshapes[shape_id]);
                            if(!def.shape_){ //TODO: What if nullptr?
                                printf("Error constructing hull shape. Subshape with id = %lu is not convex.\n", shape_id);
                                return false;
                            }

                            shape_defs.push_back(def);
                        }
                        if(shape_defs.size() != 2){
                            printf("Wrong number of sub-shapes given to Minkowski shape. Expected 2, but got %lu\n", shape_defs.size());
                            return false;
                        }
                        vshapes.push_back(new shape::Variant(shape::Minkowski(shape_defs[0], shape_defs[1])));
                    }
                    else if(shape->Attribute("type", "hull")){
                        std::vector<shape::ConvexShapeDefinition> shape_defs;
                        for(const XMLElement* shape_def_node = shape->FirstChildElement("shape_def");
                            shape_def_node != nullptr;
                            shape_def_node = shape_def_node->NextSiblingElement()
                        ){
                            shape::ConvexShapeDefinition def;
                            double x, y, z, w;

                            const XMLElement* pos_element = shape_def_node->FirstChildElement("pos");
                            pos_element->FirstChildElement("x")->QueryDoubleText(&x);
                            pos_element->FirstChildElement("y")->QueryDoubleText(&y);
                            pos_element->FirstChildElement("z")->QueryDoubleText(&z);
                            def.pos_ = clam::Vec3d(x, y, z);

                            const XMLElement* rot_element = shape_def_node->FirstChildElement("quat");
                            rot_element->FirstChildElement("x")->QueryDoubleText(&x);
                            rot_element->FirstChildElement("y")->QueryDoubleText(&y);
                            rot_element->FirstChildElement("z")->QueryDoubleText(&z);
                            rot_element->FirstChildElement("w")->QueryDoubleText(&w);
                            def.rot_ = clam::Quatd(x, y, z, w);

                            const XMLElement* size_element = shape_def_node->FirstChildElement("size");
                            if(size_element) size_element->QueryDoubleText(&def.size_);
                            else def.size_ = 1.0;

                            size_t shape_id = 0;
                            const XMLElement* shape_element = shape_def_node->FirstChildElement("shape");
                            if(shape_element){
                                shape_id = shape_element->UnsignedAttribute("id");
                                if(shape_id >= vshapes.size()){
                                    printf("Error reading convex shape. Shape at provided id not found.\n");
                                    return false;
                                }
                            }
                            def.shape_ = boost::polymorphic_get<shape::Convex>(vshapes[shape_id]);
                            if(!def.shape_){ //TODO: What if nullptr?
                                printf("Error constructing hull shape. Subshape with id = %lu is not convex.\n", shape_id);
                                return false;
                            }

                            shape_defs.push_back(def);
                        }
                        vshapes.push_back(new shape::Variant(shape::Hull(shape_defs.data(), shape_defs.size())));
                    }
                    else if(shape->Attribute("type", "complex")){
                        std::vector<shape::ConvexShapeDefinition> shape_defs;
                        for(const XMLElement* shape_def_node = shape->FirstChildElement("shape_def");
                            shape_def_node != nullptr;
                            shape_def_node = shape_def_node->NextSiblingElement()
                        ){
                            shape::ConvexShapeDefinition def;
                            double x, y, z, w;

                            const XMLElement* pos_element = shape_def_node->FirstChildElement("pos");
                            pos_element->FirstChildElement("x")->QueryDoubleText(&x);
                            pos_element->FirstChildElement("y")->QueryDoubleText(&y);
                            pos_element->FirstChildElement("z")->QueryDoubleText(&z);
                            def.pos_ = clam::Vec3d(x, y, z);

                            const XMLElement* rot_element = shape_def_node->FirstChildElement("quat");
                            rot_element->FirstChildElement("x")->QueryDoubleText(&x);
                            rot_element->FirstChildElement("y")->QueryDoubleText(&y);
                            rot_element->FirstChildElement("z")->QueryDoubleText(&z);
                            rot_element->FirstChildElement("w")->QueryDoubleText(&w);
                            def.rot_ = clam::Quatd(x, y, z, w);

                            const XMLElement* size_element = shape_def_node->FirstChildElement("size");
                            if(size_element) size_element->QueryDoubleText(&def.size_);
                            else def.size_ = 1.0;

                            size_t shape_id = 0;
                            const XMLElement* shape_element = shape_def_node->FirstChildElement("shape");
                            if(shape_element){
                                shape_id = shape_element->UnsignedAttribute("id");
                                if(shape_id >= vshapes.size()){
                                    printf("Error reading convex shape. Shape at provided id not found.\n");
                                    return false;
                                }
                            }
                            def.shape_ = boost::polymorphic_get<shape::Convex>(vshapes[shape_id]);
                            if(!def.shape_){ //TODO: What if nullptr?
                                printf("Error constructing complex shape. Subshape with id = %lu is not convex.\n", shape_id);
                                return false;
                            }

                            shape_defs.push_back(def);
                        }
                        vshapes.push_back(new shape::Variant(shape::Complex(shape_defs.data(), shape_defs.size())));
                    }
                    else{
                        printf("Unknown shape type: %s.\n", shape->Attribute("type"));
                        return false;
                    }
                }
            }

            *n_shapes_ = vshapes.size();
            *shapes_ = new const shape::Variant*[*n_shapes_];
            memcpy(*shapes_, vshapes.data(), *n_shapes_ * sizeof(shape::Variant*));

            //Load particles
            {
                const XMLElement* particles = root->FirstChildElement("particles");
                if(!particles) return false;

                config.n_part_ = 0;

                //TODO: Maybe add an attribute with the number of particles so we do not
                //have to count how many there are, ourselves.
                for(const XMLElement* particle_element = particles->FirstChildElement();
                    particle_element != nullptr;
                    particle_element = particle_element->NextSiblingElement(), ++config.n_part_)
                {}

                if(config.n_part_ == 0) return false;

                config.particles_ = new Particle[config.n_part_];

                int i = 0;
                for(const XMLElement* particle_element = particles->FirstChildElement();
                    particle_element != nullptr;
                    particle_element = particle_element->NextSiblingElement(), ++i
                ){
                    double x, y, z, w;

                    const XMLElement* pos_element = particle_element->FirstChildElement("pos");
                    pos_element->FirstChildElement("x")->QueryDoubleText(&x);
                    pos_element->FirstChildElement("y")->QueryDoubleText(&y);
                    pos_element->FirstChildElement("z")->QueryDoubleText(&z);
                    config.particles_[i].pos = clam::Vec3d(x, y, z);

                    const XMLElement* rot_element = particle_element->FirstChildElement("quat");
                    rot_element->FirstChildElement("x")->QueryDoubleText(&x);
                    rot_element->FirstChildElement("y")->QueryDoubleText(&y);
                    rot_element->FirstChildElement("z")->QueryDoubleText(&z);
                    rot_element->FirstChildElement("w")->QueryDoubleText(&w);
                    config.particles_[i].rot = clam::Quatd(x, y, z, w);

                    if(vshapes.size() > 1){
                        config.particles_[i].shape = vshapes[particle_element->FirstChildElement("shape")->UnsignedAttribute("id")];
                    }
                    else{
                        config.particles_[i].shape = vshapes[0];
                    }

                    const XMLElement* size_element = particle_element->FirstChildElement("size");
                    if(!size_element) config.particles_[i].size = 1.0;
                    else size_element->QueryDoubleText(&config.particles_[i].size);
                }
            }

            return true;
        }
    }
}

bool mch_io_xml_load_config(const char* filename, MchConfiguration* config, const MchShapeVariant*** shapes, size_t* n_shapes){
    const MCH::shape::Variant** shapes_vec;
    bool val = MCH::io::xml_load_config(filename, *config, &shapes_vec, n_shapes);

    *shapes = (const MchShapeVariant**)malloc(*n_shapes * sizeof(MchShapeVariant*));
    memcpy(*shapes, shapes_vec, *n_shapes * sizeof(MCH::shape::Variant*));
    delete[] shapes_vec;

    return val;
}

bool mch_io_xml_save_config(const char* filename, const MchConfiguration* config, const MchShapeVariant** shapes, size_t n_shapes){
    return MCH::io::xml_save_config(filename, *config, shapes, n_shapes);
}
