#ifndef MCH_IO_OBJ_LOADER_H
#define MCH_IO_OBJ_LOADER_H

#include <vector>
#include "MCH/clam/clam.h"

namespace MCH{
    namespace io{
        bool load_obj(const char* filepath, std::vector<clam::Vec3d>& vertices, std::vector<std::vector<unsigned int>>& faces);
    }
}

#endif
