#ifndef MCH_OVERLAP_GJK_H
#define MCH_OVERLAP_GJK_H

#include "MCH/clam/clam.h"

namespace MCH{

    struct Particle;
    namespace shape{
        class Convex;
    }

    namespace overlap{
        clam::Vec3d gjk_distance(const Particle&, const shape::Convex&, const Particle&, const shape::Convex&);
        bool gjk_boolean(const Particle&, const shape::Convex&, const Particle&, const shape::Convex&, double feather = 0);
        bool gjk_raycast(const Particle&, const shape::Convex&, const Particle&, const shape::Convex&, const clam::Vec3d& ray_dir, double& distance, clam::Vec3d& normal);

    }
}

#endif
