#include "MCH/overlap/overlap.h"
#include "MCH/particle.h"
#include "MCH/shape/variant.h"
#include "gjk.h"
#include "ray_casting.h"
#include "../utility.h"
#include <cstring>
#include <type_traits>

namespace MCH{
    namespace overlap{

        namespace{
            template<bool condition, typename R = void >
            using EnableIf = typename std::enable_if<condition, R>::type;

            class ShapeDistanceVisitor: public boost::static_visitor<clam::Vec3d> {
            public:
                ShapeDistanceVisitor(const Particle& pa, const Particle& pb):
                    pa_(pa), pb_(pb)
                {}

                template<typename T, typename U>
                EnableIf<std::is_base_of<shape::Convex, T>::value && std::is_base_of<shape::Convex, U>::value,
                clam::Vec3d> operator()(const T& a, const U& b)const{
                    return gjk_distance(pa_, a, pb_, b);
                }

                template<typename T>
                EnableIf<std::is_base_of<shape::Convex, T>::value,
                clam::Vec3d> operator()(const T& a, const shape::Complex& b)const{
                    const shape::ConvexShapeDefinition* shape_defs = b.shape_definitions();
                    Particle pb;
                    double min_distance = std::numeric_limits<double>::max();
                    clam::Vec3d min_dist_vec;
                    for(size_t sdid = 0; sdid < b.num_shape_definitions(); ++sdid){
                        const shape::ConvexShapeDefinition& sdef = shape_defs[sdid];
                        pb.pos  = pb_.pos + pb_.size * pb.rot.rotate(sdef.pos_);
                        pb.rot  = sdef.rot_ * pb_.rot;
                        pb.size = sdef.size_ * pb_.size;
                        clam::Vec3d dist_vec = gjk_distance(pa_, a, pb, *sdef.shape_);
                        double dist2 = dist_vec.length2();
                        if(dist2 < min_distance){
                            min_distance = dist2;
                            min_dist_vec = dist_vec;
                        }
                    }
                    return min_dist_vec;
                }

                template<typename T>
                clam::Vec3d operator()(const shape::Complex& a, const T& b)const{
                    const shape::ConvexShapeDefinition* shape_defs = a.shape_definitions();
                    Particle pa;
                    double min_distance = std::numeric_limits<double>::max();
                    clam::Vec3d min_dist_vec;
                    for(size_t sdid = 0; sdid < a.num_shape_definitions(); ++sdid){
                        const shape::ConvexShapeDefinition& sdef = shape_defs[sdid];
                        pa.pos  = pa_.pos + pa_.size * pa.rot.rotate(sdef.pos_);
                        pa.rot  = sdef.rot_ * pa_.rot;
                        pa.size = sdef.size_ * pa_.size;
                        clam::Vec3d dist_vec = (ShapeDistanceVisitor(pa, pb_))(*sdef.shape_, b);
                        double dist2 = dist_vec.length2();
                        if(dist2 < min_distance){
                            min_distance = dist2;
                            min_dist_vec = dist_vec;
                        }
                    }
                    return min_dist_vec;
                }

            private:
                const Particle& pa_;
                const Particle& pb_;
            };

            template<>
            inline clam::Vec3d ShapeDistanceVisitor::operator()(const shape::Sphere& a, const shape::Sphere& b)const{
                return pb_.pos - pa_.pos - pa_.size * a.radius() - pb_.size * b.radius();
            }

            //TODO: Move to separate header file
            template<typename T>
            static inline T sqr(T val){
                return val * val;
            }

            class ShapeOverlapVisitor: public boost::static_visitor<bool> {
            public:
                ShapeOverlapVisitor(const Particle& pa, const Particle& pb, double feather):
                    pa_(pa), pb_(pb), feather_(feather)
                {}

                template<typename T, typename U>
                EnableIf<std::is_base_of<shape::Convex, T>::value && std::is_base_of<shape::Convex, U>::value,
                bool> operator()(const T& a, const U& b)const{
                    return gjk_boolean(pa_, a, pb_, b, feather_);
                }

                template<typename T>
                EnableIf<std::is_base_of<shape::Convex, T>::value,
                bool> operator()(const T& a, const shape::Complex& b)const{
                    const shape::ConvexShapeDefinition* shape_defs = b.shape_definitions();
                    Particle pb;
                    for(size_t sdid = 0; sdid < b.num_shape_definitions(); ++sdid){
                        const shape::ConvexShapeDefinition& sdef = shape_defs[sdid];
                        pb.pos  = pb_.pos + pb_.size * pb.rot.rotate(sdef.pos_);
                        pb.rot  = sdef.rot_ * pb_.rot;
                        pb.size = sdef.size_ * pb_.size;
                        if(gjk_boolean(pa_, a, pb, *sdef.shape_, feather_)) return true;
                    }
                    return false;
                }

                template<typename T>
                bool operator()(const shape::Complex& a, const T& b)const{
                    const shape::ConvexShapeDefinition* shape_defs = a.shape_definitions();
                    Particle pa;
                    for(size_t sdid = 0; sdid < a.num_shape_definitions(); ++sdid){
                        const shape::ConvexShapeDefinition& sdef = shape_defs[sdid];
                        pa.pos  = pa_.pos + pa_.size * pa.rot.rotate(sdef.pos_);
                        pa.rot  = sdef.rot_ * pa_.rot;
                        pa.size = sdef.size_ * pa_.size;
                        if((ShapeOverlapVisitor(pa, pb_, feather_))(*sdef.shape_, b)) return true;
                    }
                    return false;
                }

            private:
                const Particle& pa_;
                const Particle& pb_;
                double feather_;
            };

            template<>
            inline bool ShapeOverlapVisitor::operator()(const shape::Sphere& a, const shape::Sphere& b)const{
                return (pb_.pos - pa_.pos).length2() < sqr(pa_.size * a.radius() + pb_.size * b.radius() + feather_);
            }

            class ShapeRaycastVisitor: public boost::static_visitor<bool> {
            public:
                ShapeRaycastVisitor(const Particle& pa, const Particle& pb, const clam::Vec3d& ray_dir, double& distance, clam::Vec3d& normal):
                    pa_(pa), pb_(pb), ray_dir_(ray_dir), dist_(distance), normal_(normal)
                {}

                template<typename T, typename U>
                EnableIf<std::is_base_of<shape::Convex, T>::value && std::is_base_of<shape::Convex, U>::value,
                bool> operator()(const T& a, const U& b)const{
                    return gjk_raycast(pa_, a, pb_, b, ray_dir_, dist_, normal_);
                }

                template<typename T>
                EnableIf<std::is_base_of<shape::Convex, T>::value,
                bool> operator()(const T& a, const shape::Complex& b)const{
                    const shape::ConvexShapeDefinition* shape_defs = b.shape_definitions();
                    Particle pb;
                    for(size_t sdid = 0; sdid < b.num_shape_definitions(); ++sdid){
                        const shape::ConvexShapeDefinition& sdef = shape_defs[sdid];
                        pb.pos  = pb_.pos + pb_.size * pb.rot.rotate(sdef.pos_);
                        pb.rot  = sdef.rot_ * pb_.rot;
                        pb.size = sdef.size_ * pb_.size;
                        if(gjk_raycast(pa_, a, pb, *sdef.shape_, ray_dir_, dist_, normal_)) return true;
                    }
                    return false;
                }

                template<typename T>
                bool operator()(const shape::Complex& a, const T& b)const{
                    const shape::ConvexShapeDefinition* shape_defs = a.shape_definitions();
                    Particle pa;
                    for(size_t sdid = 0; sdid < a.num_shape_definitions(); ++sdid){
                        const shape::ConvexShapeDefinition& sdef = shape_defs[sdid];
                        pa.pos  = pa_.pos + pa_.size * pa.rot.rotate(sdef.pos_);
                        pa.rot  = sdef.rot_ * pa_.rot;
                        pa.size = sdef.size_ * pa_.size;
                        if((ShapeRaycastVisitor(pa, pb_, ray_dir_, dist_, normal_))(*sdef.shape_, b)) return true;
                    }
                    return false;
                }

            private:
                const Particle& pa_;
                const Particle& pb_;
                const clam::Vec3d& ray_dir_;
                double& dist_;
                clam::Vec3d& normal_;
            };

            template<>
            inline bool ShapeRaycastVisitor::operator()(const shape::Sphere& a, const shape::Sphere& b)const{
                clam::Vec3d normal;
                return sphere_raycast(pa_.size * a.radius() + pb_.size * b.radius(), pa_.pos - pb_.pos, ray_dir_, dist_, normal);
            }
        }//namespace detail

        clam::Vec3d shape_distance(const Particle& pa, const shape::Variant& a, const Particle& pb, const shape::Variant& b){
            return boost::apply_visitor(ShapeDistanceVisitor(pa, pb), a, b);
        }

        bool shape_overlap(const Particle& pa, const shape::Variant& a, const Particle& pb, const shape::Variant& b, double feather){
            return boost::apply_visitor(ShapeOverlapVisitor(pa, pb, feather), a, b);
        }

        bool shape_raycast(const Particle& pa, const shape::Variant& a, const Particle& pb, const shape::Variant& b, const clam::Vec3d& ray_dir, double& distance, clam::Vec3d& normal){
            return boost::apply_visitor(ShapeRaycastVisitor(pa, pb, ray_dir, distance, normal), a, b);
        }

    }
}

void mch_shape_distance(const MchParticle* pa, const MchShapeVariant* sa, const MchParticle* pb, const MchShapeVariant* sb, double* distance){
    auto dist = MCH::overlap::shape_distance(*pa, *sa, *pb, *sb);
    memcpy(distance, dist.data(), 3 * sizeof(double));
}

bool mch_shape_overlap(const MchParticle* pa, const MchShapeVariant* sa, const MchParticle* pb, const MchShapeVariant* sb, double feather){
    return MCH::overlap::shape_overlap(*pa, *sa, *pb, *sb, feather);
}

bool mch_shape_raycast(const MchParticle* pa, const MchShapeVariant* sa, const MchParticle* pb, const MchShapeVariant* sb, const double* ray_dir, double* distance, double* normal){
    clam::Vec3d normal_;
    bool result = MCH::overlap::shape_raycast(*pa, *sa, *pb, *sb, clam::Vec3d(ray_dir), *distance, normal_);
    memcpy(normal, normal_.data(), 3 * sizeof(double));
    return result;
}

