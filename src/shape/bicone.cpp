#include "MCH/shape/bicone.h"
#include "../utility.h"
#include <algorithm>

namespace MCH{
    namespace shape{

        Bicone::Bicone(double base_radius, double height):
            base_radius_(base_radius), half_height_(0.5 * height),
            sintheta_(base_radius_ / sqrt(sqr(base_radius) + sqr(half_height_))),
            in_radius_(sintheta_ * half_height_), out_radius_(std::max(base_radius_, half_height_)),
            volume_(M_PI * sqr(base_radius) * height / 3.0)
        {}

    }
}


MchShapeBicone* mch_shape_make_bicone(double base_radius, double height){
    return static_cast<MchShapeBicone*>(new MCH::shape::Bicone(base_radius, height));
}

void mch_shape_delete_bicone(MchShapeBicone* cone){
    auto cpp_cone = static_cast<MCH::shape::Bicone*>(cone);
    delete cpp_cone;
    cone = nullptr;
}
