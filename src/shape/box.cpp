#include "MCH/shape/box.h"
#include "../utility.h"
#include <algorithm>

namespace MCH{
    namespace shape{

        Box::Box(const clam::Vec3d& dims):
            extent_(0.5 * dims),
            in_radius_(std::min(extent_[0], std::min(extent_[1], extent_[2]))), out_radius_(extent_.length()),
            volume_(dims[0] * dims[1] * dims[2])
        {}

    }
}

MchShapeBox* mch_shape_make_box(double hx, double hy, double hz){
    return static_cast<MchShapeBox*>(new MCH::shape::Box(clam::Vec3d(hx, hy, hz)));
}

void mch_shape_delete_box(MchShapeBox* box){
    auto cpp_box = static_cast<MCH::shape::Box*>(box);
    delete cpp_box;
    box = nullptr;
}

