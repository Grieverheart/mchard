#include "MCH/shape/complex.h"
#include "MCH/shape/convex.h"

namespace MCH{
    namespace shape{

        Complex::Complex(const ConvexShapeDefinition* shape_defs, const size_t n_shape_defs):
            shape_defs_(new ConvexShapeDefinition[n_shape_defs]), n_shape_defs_(n_shape_defs),
            out_radius_(0.0), volume_(0.0)
        {
            for(size_t i = 0; i < n_shape_defs_; ++i){
                shape_defs_[i] = shape_defs[i];

                double max_dist = shape_defs_[i].pos_.length() + shape_defs_[i].size_ * shape_defs_[i].shape_->out_radius();
                if(max_dist > out_radius_) out_radius_ = max_dist;
                volume_ += pow(shape_defs_[i].size_, 3.0) * shape_defs_[i].shape_->volume();
            }
        }

        Complex::Complex(const Complex& other):
            shape_defs_(new ConvexShapeDefinition[other.n_shape_defs_]), n_shape_defs_(other.n_shape_defs_),
            out_radius_(other.out_radius_), volume_(other.volume_)
        {
            for(size_t i = 0; i < n_shape_defs_; ++i){
                shape_defs_[i] = other.shape_defs_[i];
            }
        }

        Complex::~Complex(void){
            delete[] shape_defs_;
        }

        const ConvexShapeDefinition* Complex::shape_definitions(void)const{
            return shape_defs_;
        }

        const size_t Complex::num_shape_definitions(void)const{
            return n_shape_defs_;
        }

        double Complex::in_radius(void)const{
            return 0.0;
        }

        double Complex::out_radius(void)const{
            return out_radius_;
        }

        double Complex::volume(void)const{
            return volume_;
        }
    }
}
