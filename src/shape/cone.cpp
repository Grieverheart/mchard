#include "MCH/shape/cone.h"
#include "../utility.h"

namespace MCH{
    namespace shape{

        Cone::Cone(double base_radius, double height):
            base_radius_(base_radius), half_height_(0.5 * height),
            sintheta_(base_radius_ / sqrt(sqr(base_radius) + sqr(height))),
            in_radius_(((sintheta_ < 1.0)? sintheta_: 1.0) * half_height_), out_radius_(sqrt(sqr(base_radius_) + sqr(half_height_))),
            volume_(M_PI * sqr(base_radius) * height / 3.0)
        {}

    }
}

MchShapeCone* mch_shape_make_cone(double base_radius, double height){
    return static_cast<MchShapeCone*>(new MCH::shape::Cone(base_radius, height));
}

void mch_shape_delete_cone(MchShapeCone* cone){
    auto cpp_cone = static_cast<MCH::shape::Cone*>(cone);
    delete cpp_cone;
    cone = nullptr;
}

