#include "MCH/shape/cylinder.h"
#include "../utility.h"
#include <algorithm>

namespace MCH{
    namespace shape{

        Cylinder::Cylinder(double base_radius, double height):
            base_radius_(base_radius), half_height_(0.5 * height),
            in_radius_(std::min(half_height_, base_radius_)), out_radius_(sqrt(sqr(half_height_) + sqr(base_radius_))),
            volume_(M_PI * sqr(base_radius) * height)
        {}

    }
}

// -- C implementation
MchShapeCylinder* mch_shape_make_cylinder(double base_radius, double height){
    return static_cast<MchShapeCylinder*>(new MCH::shape::Cylinder(base_radius, height));
}

void mch_shape_delete_cylinder(MchShapeCylinder* cylinder){
    auto cpp_cylinder = static_cast<MCH::shape::Cylinder*>(cylinder);
    delete cpp_cylinder;
    cylinder = nullptr;
}

