#include "MCH/shape/hull.h"

namespace MCH{
    namespace shape{

        Hull::Hull(const ConvexShapeDefinition* shape_defs, const size_t n_shape_defs, double volume):
            shape_defs_(new ConvexShapeDefinition[n_shape_defs]), n_shape_defs_(n_shape_defs),
            out_radius_(0.0), volume_(volume)
        {
            for(size_t i = 0; i < n_shape_defs_; ++i){
                shape_defs_[i] = shape_defs[i];
                double outradius = shape_defs_[i].pos_.length() + shape_defs_[i].size_ * shape_defs_[i].shape_->out_radius();
                if(outradius > out_radius_) out_radius_ = outradius;
            }
        }

        Hull::Hull(const Hull& other):
            shape_defs_(new ConvexShapeDefinition[other.n_shape_defs_]), n_shape_defs_(other.n_shape_defs_),
            out_radius_(other.out_radius_), volume_(other.volume_)
        {
            for(size_t i = 0; i < n_shape_defs_; ++i){
                shape_defs_[i] = other.shape_defs_[i];
            }
        }

        Hull::~Hull(void){
            delete[] shape_defs_;
        }

        const ConvexShapeDefinition* Hull::shape_definitions(void)const{
            return shape_defs_;
        }

        const size_t Hull::num_shape_definitions(void)const{
            return n_shape_defs_;
        }

        double Hull::in_radius(void)const{
            return 0.0;
        }

        double Hull::out_radius(void)const{
            return out_radius_;
        }

        double Hull::volume(void)const{
            return volume_;
        }
    }
}
