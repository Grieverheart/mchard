#include "MCH/shape/leaf_cylinder.h"
#include "../utility.h"
#include <algorithm>

namespace MCH{
    namespace shape{
        LeafCylinder::LeafCylinder(double width, double length, double height):
            half_width_(0.5 * width), half_length_(0.5 * length), half_height_(0.5 * height),
            circle_radius_(0.25 * (sqr(length) + sqr(width)) / width), circle_distance_(0.25 * (sqr(length) - sqr(width)) / width),
            in_radius_(std::min(half_height_, half_width_)), out_radius_(sqrt(sqr(half_height_) + sqr(half_length_)))
        {
            double theta = 2.0 * asin(half_length_ / circle_radius_);
            volume_ = height * sqr(circle_radius_) * (theta - sin(theta));
        }

    }
}
