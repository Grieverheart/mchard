#include "MCH/shape/minkowski.h"

namespace MCH{
    namespace shape{

        Minkowski::Minkowski(const ConvexShapeDefinition& pa, const ConvexShapeDefinition& pb, double volume):
            pa_(pa), pb_(pb),
            inv_rot_a_(pa_.rot_.inv()), inv_rot_b_(pa_.rot_.inv()),
            //NOTE: The circumscribed and inscribed radii calculated here, are upper and
            //lower bounds respectively.
            in_radius_(pa.size_ * pa.shape_->in_radius() + pb.size_ * pb.shape_->in_radius()),
            out_radius_(pa.size_ * pa.shape_->out_radius() + pb.size_ * pb.shape_->out_radius()),
            volume_(volume)
        {}

        double Minkowski::in_radius(void)const{
            return in_radius_;
        }
        double Minkowski::out_radius(void)const{
            return out_radius_;
        }
        double Minkowski::volume(void)const{
            return volume_;
        }
        const ConvexShapeDefinition& Minkowski::shape_def_a(void)const{
            return pa_;
        }
        const ConvexShapeDefinition& Minkowski::shape_def_b(void)const{
            return pb_;
        }

    }
}

//TODO: Implement these.
//MchShapeMinkowski* mch_shape_make_cone(double base_radius, double height){
//    return static_cast<MchShapeMinkowski*>(new MCH::shape::Minkowski(base_radius, height));
//}
//
//void mch_shape_delete_cone(MchShapeMinkowski* minkowski){
//    auto cpp_minkowski = static_cast<MCH::shape::Minkowski*>(minkowski);
//    delete cpp_minkowski;
//    minkowski = nullptr;
//}

