#include "MCH/shape/sphere-swept.h"

namespace MCH{
    namespace shape{

        SphereSwept::SphereSwept(const Convex* shape, double radius):
            shape_(shape), radius_(radius),
            in_radius_(shape_->in_radius() + radius_),
            out_radius_(shape_->out_radius() + radius_)
        {}

        double SphereSwept::in_radius(void)const{
            return in_radius_;
        }
        double SphereSwept::out_radius(void)const{
            return out_radius_;
        }
        double SphereSwept::volume(void)const{
            return 1.0;
        }

        double SphereSwept::radius(void)const{
            return radius_;
        }

        const Convex* SphereSwept::shape(void)const{
            return shape_;
        }
    }
}

//TODO: Implement these.
//MchShapeSphereSwept* mch_shape_make_cone(double base_radius, double height){
//    return static_cast<MchShapeSphereSwept*>(new MCH::shape::SphereSwept(base_radius, height));
//}
//
//void mch_shape_delete_cone(MchShapeSphereSwept* minkowski){
//    auto cpp_minkowski = static_cast<MCH::shape::SphereSwept*>(minkowski);
//    delete cpp_minkowski;
//    minkowski = nullptr;
//}

