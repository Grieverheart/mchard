#include "MCH/shape/sphere.h"

MchShapeSphere* mch_shape_make_sphere(void){
    return static_cast<MchShapeSphere*>(new MCH::shape::Sphere());
}

void mch_shape_delete_sphere(MchShapeSphere* sphere){
    auto cpp_sphere = static_cast<MCH::shape::Sphere*>(sphere);
    delete cpp_sphere;
    sphere = nullptr;
}

