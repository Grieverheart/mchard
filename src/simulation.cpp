#define DSFMT_MEXP 19937
#define HAVE_SSE2
#include "dSFMT.h"

#include "MCH/simulation.h"
#include "MCH/CellList.h"
#include "MCH/particle.h"
#include "MCH/box/Base.h"
#include "MCH/shape/variant.h"
#include "MCH/overlap/overlap.h"
#include "overlap/ray_casting.h"
#include "utility.h"
#include <ctime>
#include <cstring>
#include <cmath>
#include <limits>
#include <memory>
#include <algorithm>

#define rand01() dsfmt_genrand_close_open(rng_)
#define randm11() (2.0 * dsfmt_genrand_open_open(rng_) - 1.0)
#define rand_int(a) uint32_t((a) * dsfmt_genrand_close_open(rng_))

namespace MCH{

using namespace overlap;

namespace{
    template<typename F>
    inline bool foreach_pair(const CellList& cll_, F func){
        for(auto cell: cll_.cells()){
            for(auto cell_nb: cll_.cell_vol_nbs(cell)){
                if(cell == cell_nb){
                    for(auto pa_itr = cll_.cell_content(cell); pa_itr != pa_itr.end(); ++pa_itr){
                        for(auto pb_itr = pa_itr + 1; pb_itr != pb_itr.end(); ++pb_itr){
                            if(func(*pa_itr, *pb_itr)) return true;
                        }
                    }
                }
                else{
                    for(auto pa: cll_.cell_content(cell)){
                        for(auto pb: cll_.cell_content(cell_nb)){
                            if(func(pa, pb)) return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    class ShapeInRadiusVisitor: public boost::static_visitor<double>{
    public:
        template<typename T>
        double operator()(const T& conex_shape)const{
            return conex_shape.in_radius();
        }

        double operator()(const shape::Sphere& sph)const{
            return sph.radius();
        }
    };

    class ShapeOutRadiusVisitor: public boost::static_visitor<double>{
    public:
        template<typename T>
        double operator()(const T& convex_shape)const{
            return convex_shape.out_radius();
        }

        double operator()(const shape::Sphere& sph)const{
            return sph.radius();
        }
    };

    static inline double min_overlap_distance(double size_a, const shape::Variant& sa, double size_b, const shape::Variant& sb){
        return size_a * boost::apply_visitor(ShapeInRadiusVisitor(), sa) +
               size_b * boost::apply_visitor(ShapeInRadiusVisitor(), sb);
    }

    static inline double max_overlap_distance(double size_a, const shape::Variant& sa, double size_b, const shape::Variant& sb){
        return size_a * boost::apply_visitor(ShapeOutRadiusVisitor(), sa) +
               size_b * boost::apply_visitor(ShapeOutRadiusVisitor(), sb);
    }
}

//TODO: What if configuration is empty? Headaches!!
Simulation::Simulation(const Configuration& config):
    config_(config),
    cll_(new CellList()),
    rng_(new dsfmt_t),
    rcm_drift_(0.0),
    pressure_(100.0)
{
    Particle* particles_  = config_.particles_;
    const box::Base* box_ = config_.box_;
    size_t n_part_        = config_.n_part_;

    //TODO: We should perform this test somewhere, but not sure if we should perform it here
    if(box_->is_hard()){
        for(size_t i = 0; i < n_part_; ++i){
            const shape::Variant* shape = particles_[i].shape;
            if(box_->check_overlap(particles_[i], *shape)){
                printf("Warning: particle with id = %lu, outside box confines\n", i);
                printf("\tposition: %e, %e, %e\n", particles_[i].pos[0], particles_[i].pos[1], particles_[i].pos[2]);
            }
        }
    }

    //find minimum cell size by iterating over all particles and checking the size of their bounding sphere
    double min_cell_size = 0.0;
    for(size_t i = 0; i < n_part_; ++i){
        double out_radius = particles_[i].size * boost::apply_visitor(ShapeOutRadiusVisitor(), *particles_[i].shape);
        if(out_radius > min_cell_size) min_cell_size = out_radius;
    }
    min_cell_size *= 2.0;

    cll_->init(n_part_, box_->bounds(), min_cell_size * 1.0001); //@note: Add some small value to min_cell_size to avoid errors

    //Make sure particles are inside the box
    for(size_t i = 0; i < n_part_; ++i) particles_[i].pos = box_->apply_pbc(particles_[i].pos);

    //Sort particles by cell index to improve cache coherency.
    std::sort(particles_, particles_ + n_part_, [this](const Particle& pa, const Particle& pb) -> bool {
        int cid_a = cll_->cellIndex(pa.pos);
        int cid_b = cll_->cellIndex(pb.pos);
        return cid_a < cid_b;
    });

    //Add particles to cell list
    for(size_t i = 0; i < n_part_; ++i) cll_->add(i, particles_[i].pos);

    dsfmt_init_gen_rand(rng_, time(NULL));

    //foreach_pair(*cll_, [this](int pa, int pb) -> bool {
    //    Particle* particles_  = config_.particles_;
    //    const box::Base* box_ = config_.box_;
    //    const shape::Variant& shape_a = *particles_[pa].shape;
    //    const shape::Variant& shape_b = *particles_[pb].shape;
    //    clam::Vec3d dist = box_->minimum_image(particles_[pb].pos - particles_[pa].pos);
    //
    //    if(dist.length2() < sqr(min_overlap_distance(particles_[pa].size, shape_a, particles_[pb].size, shape_b))){
    //        printf("overlap!\n");
    //        return false;
    //    }
    //    else if(dist.length2() < sqr(max_overlap_distance(particles_[pa].size, shape_a, particles_[pb].size, shape_b))){
    //        Particle ppa = particles_[pa];
    //        Particle ppb = particles_[pb];
    //        ppa.pos = clam::Vec3d(0.0);
    //        ppb.pos = dist;
    //        bool overlap = shape_overlap(ppa, shape_a, ppb, shape_b);
    //        if(overlap) printf("overlap.\n");
    //        if(overlap) return false;
    //    }
    //    return false;
    //});
}

Simulation::~Simulation(void){
    delete cll_;
    delete rng_;
}

bool Simulation::rotate_particle(size_t r_pid, const clam::Quatd& q_rotation){
    Particle* particles_  = config_.particles_;
    const box::Base* box_ = config_.box_;

    Particle pa = particles_[r_pid];
    //TODO: In principle, this is not the correct order of applying a rotation.
    pa.rot = pa.rot * q_rotation;

    const shape::Variant& shape_a = *pa.shape;

    if(box_->is_hard()){
        if(box_->check_overlap(pa, shape_a)) return false;
    }

    pa.pos = clam::Vec3d(0.0);

    /* Check for overlaps */
    for(auto cid_itr = cll_->particle_cell_nbs(r_pid); cid_itr != cid_itr.end(); ++cid_itr){
        for(auto pid: cll_->cell_content(*cid_itr)){
            //TODO: Fix integer types
            if(pid != int(r_pid)){
                const shape::Variant& shape_b = *particles_[pid].shape;
                clam::Vec3d dist = box_->minimum_image(particles_[pid].pos - particles_[r_pid].pos);
                Particle pb = particles_[pid];
                pb.pos = dist;

                if(dist.length2() < sqr(min_overlap_distance(pa.size, shape_a, pb.size, shape_b))){
                    //Reorder neighboring cells for coherence
                    cll_->push_front(cid_itr);
                    return false;
                }
                else if(dist.length2() < sqr(max_overlap_distance(pa.size, shape_a, pb.size, shape_b))){
                    bool overlap = shape_overlap(pa, shape_a, pb, shape_b);
                    if(overlap){
                        //Reorder neighboring cells for coherence
                        cll_->push_front(cid_itr);
                        return false;
                    }
                }
            }
        }
    }

    particles_[r_pid].rot = pa.rot;

    return true;
}

bool Simulation::move_particle(size_t r_pid, const clam::Vec3d& dx){
    Particle* particles_  = config_.particles_;
    const box::Base* box_ = config_.box_;

    Particle pa = particles_[r_pid];

    auto new_pos = box_->apply_pbc(particles_[r_pid].pos + dx);

    const shape::Variant& shape_a = *pa.shape;

    if(box_->is_hard()){
        pa.pos = new_pos;
        if(box_->check_overlap(pa, shape_a)) return false;
    }

    pa.pos = clam::Vec3d(0.0);

    /* Check for overlaps */
    for(auto cid_itr = cll_->particle_cell_nbs(new_pos); cid_itr != cid_itr.end(); ++cid_itr){
        for(auto pid: cll_->cell_content(*cid_itr)){
            //TODO: Fix integer types
            if(pid != int(r_pid)){
                const shape::Variant& shape_b = *particles_[pid].shape;
                clam::Vec3d dist = box_->minimum_image(particles_[pid].pos - new_pos);
                Particle pb = particles_[pid];
                pb.pos = dist;

                if(dist.length2() < sqr(min_overlap_distance(pa.size, shape_a, pb.size, shape_b))){
                    //Reorder neighboring cells for coherence
                    cll_->push_front(cid_itr);
                    return false;
                }
                else if(dist.length2() < sqr(max_overlap_distance(pa.size, shape_a, pb.size, shape_b))){
                    bool overlap = shape_overlap(pa, shape_a, pb, shape_b);
                    if(overlap){
                        //Reorder neighboring cells for coherence
                        cll_->push_front(cid_itr);
                        return false;
                    }
                }
            }
        }
    }

    rcm_drift_ += dx / config_.n_part_;
    particles_[r_pid].pos = new_pos;
    cll_->update(r_pid, new_pos);

    return true;
}

//TODO: Perhaps need to rotate using nematic director.
bool Simulation::cluster_rotate(size_t r_pid, const clam::Quatd& rotation, double cutoff_dist){
    Particle* particles_  = config_.particles_;
    const box::Base* box_ = config_.box_;

    double cutoff2 = sqr(cutoff_dist);

    std::vector<int> pids;
    pids.push_back(r_pid);
    //Create cluster
    clam::Vec3d r_cm(0.0);
    for(auto cid_itr = cll_->particle_cell_nbs(r_pid); cid_itr != cid_itr.end(); ++cid_itr){
        for(size_t pid: cll_->cell_content(*cid_itr)){
            if(pid == r_pid) continue;
            clam::Vec3d dist = box_->minimum_image(particles_[pid].pos - particles_[r_pid].pos);
            if(dist.length2() < cutoff2){
                pids.push_back(pid);
                r_cm += dist;
            }
        }
    }

    if(pids.size() > 10) return false;

    r_cm = r_cm / pids.size() + particles_[r_pid].pos;

    std::vector<Particle> backup_particles;
    for(size_t pid_a: pids){
        Particle pa = particles_[pid_a];
        const shape::Variant& shape_a = *pa.shape;

        //NOTE: I think that we don't need to set pa.pos = 0.0. GJK should be able to handle
        //cases where particle a is not at the origin. In that case, we can set pb.pos = pa.pos + min_dist
        clam::Vec3d dist = box_->minimum_image(pa.pos - r_cm);
        clam::Vec3d pos_a = box_->apply_pbc(rotation.rotate(dist) + r_cm);
        pa.rot = rotation * pa.rot;

        if(box_->is_hard()){
            pa.pos = pos_a;
            if(box_->check_overlap(pa, shape_a)) return false;
        }

        pa.pos = 0.0;

        /* Check for overlaps */
        //NOTE: We could in principle also store the new cell id.
        for(auto cid_itr = cll_->particle_cell_nbs(pos_a); cid_itr != cid_itr.end(); ++cid_itr){
            for(size_t pid_b: cll_->cell_content(*cid_itr)){
                //Check if the particle belongs to the cluster. If it does, we don't have
                //to check for overlaps. This also implicitly checks if pid_b == pid_a.
                bool should_skip = false;
                for(size_t pid: pids){
                    if(pid_b == pid){
                        should_skip = true;
                        break;
                    }
                }
                if(should_skip) continue;

                Particle pb = particles_[pid_b];
                const shape::Variant& shape_b = *pb.shape;
                clam::Vec3d dist = box_->minimum_image(pb.pos - pos_a);
                pb.pos = dist;

                if(dist.length2() < sqr(min_overlap_distance(pa.size, shape_a, pb.size, shape_b))){
                    return false;
                }
                else if(dist.length2() < sqr(max_overlap_distance(pa.size, shape_a, pb.size, shape_b))){
                    bool overlap = shape_overlap(pa, shape_a, pb, shape_b);
                    if(overlap) return false;
                }
            }
        }
        pa.pos = pos_a;
        backup_particles.push_back(pa);
    }

    for(size_t n = 0; n < pids.size(); ++n){
        particles_[pids[n]] = backup_particles[n];
        cll_->update(pids[n], particles_[pids[n]].pos);
        //TODO:
        //rcm_drift_ += dx / config_.n_part_;
    }

    return true;
}

//NOTE: Something's wrong with this one.
bool Simulation::event_chain_move(size_t r_pid, double length, const clam::Vec3d& rdir){
    Particle* particles_  = config_.particles_;
    const box::Base* box_ = config_.box_;

    auto dir = rdir / rdir.length();

    size_t p_current  = r_pid;
    size_t p_previous = p_current;

    double dist_remaining = length;
    while(dist_remaining > 0.0){
        Particle pa = particles_[p_current];
        const shape::Variant& shape_a = *pa.shape;

        double min_dist = dist_remaining;

        //NOTE: First checking for wall collision is slightly faster
        clam::Vec3d normal;
        if(box_->is_hard()){
            double t = min_dist;
            if(box_->raycast(pa, shape_a, dir, t, normal)){
                //TODO: Maybe not the best way to avoid wall overlaps
                t -= 1.0e-12;
                if(t > 0.0) min_dist = t;
            }
        }

        size_t p_min = 0;
        bool overlap = false;
        /* Check for overlaps */
        for(auto cid_itr = cll_->particle_cell_nbs(p_current); cid_itr != cid_itr.end(); ++cid_itr){
            for(auto pid: cll_->cell_content(*cid_itr)){
                //TODO: Fix integer types
                if(pid != int(p_current) && pid != int(p_previous)){
                    const shape::Variant& shape_b = *particles_[pid].shape;
                    clam::Vec3d dist = box_->minimum_image(particles_[pid].pos - pa.pos);

                    double radius = max_overlap_distance(pa.size, shape_a, particles_[pid].size, shape_b);
                    double t = min_dist;
                    clam::Vec3d temp_normal;

                    if(dist.length2() < sqr(radius) || (sphere_raycast(radius, dist, dir, t, temp_normal) && t < min_dist)){
                        Particle pb = particles_[pid];
                        pb.pos = pa.pos + dist;
                        double t = min_dist;
                        clam::Vec3d normal;
                        if(shape_raycast(pa, shape_a, pb, shape_b, dir, t, normal)){
                            //if(t == 0.0){
                            //    printf("Overlap: %d!\n", overlap);
                            //    printf("p_current: %lu, pid: %d, p_previous: %lu\n", p_current, pid, p_previous);
                            //    printf("Box overlap, a: %d, b: %d\n", box_->check_overlap(pa, shape_a), box_->check_overlap(pb, shape_b));
                            //    printf("Overlap, a-b: %d\n", shape_overlap(pa, shape_a, pb, shape_b));
                            //    printf("Distance, a-b: %f\n", shape_distance(pa, shape_a, pb, shape_b).length());
                            //    printf("min_dist: %e, dist_remaining: %e\n", min_dist, dist_remaining);
                            //    printf("Direction: %e, %e, %e\n", dir[0], dir[1], dir[2]);
                            //    printf("Relative Distance: %e, %e, %e\n", dist[0], dist[1], dist[2]);
                            //    double angle;
                            //    clam::Vec3d axis;
                            //    pa.rot.toAxisAngle(angle, axis);
                            //    printf("Rot_a: %e, glm::vec3(%e, %e, %e)\n", angle, axis[0], axis[1], axis[2]);
                            //    pb.rot.toAxisAngle(angle, axis);
                            //    printf("Rot_b: %e, glm::vec3(%e, %e, %e)\n", angle, axis[0], axis[1], axis[2]);
                            //}
                            min_dist = t;
                            p_min   = pid;
                            overlap = true;
                        }
                    }
                }
            }
        }


        int current_cell = cll_->cell_index(p_current);
        auto p_pos = box_->minimum_image(pa.pos - cll_->cell_origin(current_cell));
        double cell_cross_dist;
        int cell_offset = cell_raycast(cll_->cell_size(), p_pos, dir, cell_cross_dist);
        while(cell_cross_dist < min_dist){
            current_cell = cll_->cell_index_at_offset(current_cell, cell_offset);
            for(auto cid_itr = cll_->cell_dir_nbs(current_cell, cell_offset); cid_itr != cid_itr.end(); ++cid_itr){
                for(auto pid: cll_->cell_content(*cid_itr)){
                    if(pid != int(p_current) && pid != int(p_previous)){
                        const shape::Variant& shape_b = *particles_[pid].shape;
                        clam::Vec3d dist = box_->minimum_image(particles_[pid].pos - pa.pos);

                        double radius = max_overlap_distance(pa.size, shape_a, particles_[pid].size, shape_b);
                        double t = min_dist;
                        clam::Vec3d normal;

                        if(dist.length2() < sqr(radius) || (sphere_raycast(radius, dist, dir, t, normal) && t < min_dist)){
                            Particle pb = particles_[pid];
                            pb.pos = pa.pos + dist;
                            clam::Vec3d normal;
                            double t = min_dist;
                            if(shape_raycast(pa, shape_a, pb, shape_b, dir, t, normal)){
                                //if(t == 0.0){
                                //    printf("Overlap: %d!\n", overlap);
                                //    printf("p_current: %lu, pid: %d, p_previous: %lu\n", p_current, pid, p_previous);
                                //    printf("Box overlap, a: %d, b: %d\n", box_->check_overlap(pa, shape_a), box_->check_overlap(pb, shape_b));
                                //    printf("Overlap, a-b: %d\n", shape_overlap(pa, shape_a, pb, shape_b));
                                //    printf("Distance, a-b: %f\n", shape_distance(pa, shape_a, pb, shape_b).length());
                                //    printf("min_dist: %e, dist_remaining: %e\n", min_dist, dist_remaining);
                                //    printf("Direction: %e, %e, %e\n", dir[0], dir[1], dir[2]);
                                //    printf("Relative Distance: %e, %e, %e\n", dist[0], dist[1], dist[2]);
                                //    double angle;
                                //    clam::Vec3d axis;
                                //    pa.rot.toAxisAngle(angle, axis);
                                //    printf("Rot_a: %e, glm::vec3(%e, %e, %e)\n", angle, axis[0], axis[1], axis[2]);
                                //    pb.rot.toAxisAngle(angle, axis);
                                //    printf("Rot_b: %e, glm::vec3(%e, %e, %e)\n", angle, axis[0], axis[1], axis[2]);
                                //}
                                p_min = pid;
                                t = min_dist;
                                overlap = true;
                            }
                        }
                    }
                }
            }
            auto p_pos = box_->minimum_image(pa.pos - cll_->cell_origin(current_cell));
            double new_cell_cross_dist;
            cell_offset = cell_raycast(cll_->cell_size(), p_pos, dir, new_cell_cross_dist);
            //NOTE: When not using periodic boundary conditions
            if(new_cell_cross_dist < cell_cross_dist) break;
            cell_cross_dist = new_cell_cross_dist;
        }

        double move_dist = std::min(min_dist, dist_remaining);
        //printf("%f, %f\n", min_dist, move_dist);
        dist_remaining -= move_dist;
        particles_[p_current].pos = box_->apply_pbc(particles_[p_current].pos + dir * std::max(0.0, move_dist - 1.0e-6));
        cll_->update(p_current, particles_[p_current].pos);

        p_previous = p_current;

        if(!overlap) dir = dir - 2.0 * clam::dot(dir, normal) * normal;
        else p_current = p_min;
    }

    rcm_drift_ += (dir * length) / config_.n_part_;

    return true;
}

bool Simulation::change_volume(const clam::Vec3d& ds){
    Particle* particles_ = config_.particles_;
    box::Base*& box_     = config_.box_;
    size_t n_part_       = config_.n_part_;

    auto old_box = box_->clone();

    clam::Vec3d factor = (ds / old_box->bounds()) + 1.0;
    box_->rescale(factor);

    double new_volume = box_->volume();
    if(!box_->is_hard()){
        double min_cell_size = cll_->cell_min_size();
        clam::Vec3d bounds = box_->bounds();
        for(size_t d = 0; d < 3; ++d){
            if(bounds[d] < min_cell_size) return false;
        }
    }

    double old_volume = old_box->volume();
    double dv = new_volume - old_volume;

    double fraction = new_volume / old_volume;

    if(rand01() <= exp(-pressure_ * dv + n_part_ * log(fraction))){
        //Copy the old box for backup
        //Change the box size to the new volume

        auto old_positions = std::unique_ptr<clam::Vec3d[]>(new clam::Vec3d[n_part_]);
        for(size_t i = 0; i < n_part_; ++i){
            old_positions[i] = particles_[i].pos;
            particles_[i].pos *= factor;
        }

        auto cll_old = cll_;
        bool cll_rebuild = true;//cll_->should_rebuild(factor);
        if(cll_rebuild){
            cll_ = new CellList();
            cll_->init(n_part_, box_->bounds(), cll_old->cell_min_size());
            for(size_t i = 0; i < n_part_; ++i) cll_->add(i, particles_[i].pos);
        }
        //TODO: I don't scale back when rejecting!!!
        else cll_->rescale(factor);

        bool should_check_collisions = (ds[0] < 0.0 || ds[1] < 0.0 || ds[2] < 0.0);

        //For hard boxes, if we shrink we need to check overlaps with the box
        if(box_->is_hard() && should_check_collisions){
            for(size_t i = 0; i < n_part_; ++i){
                if(box_->check_overlap(particles_[i], *particles_[i].shape)){
                    auto temp_box = box_;
                    box_ = old_box;
                    delete temp_box;
                    if(cll_rebuild){
                        auto temp_cll = cll_;
                        cll_ = cll_old;
                        delete temp_cll;
                    }
                    for(size_t i = 0; i < n_part_; ++i) particles_[i].pos = old_positions[i];
                    return false;
                }
            }
        }

        //If we shrink we need to check for collisions
        if(should_check_collisions &&
            foreach_pair(*cll_, [this, particles_, box_](int pa, int pb) -> bool {
                const shape::Variant& shape_a = *particles_[pa].shape;
                const shape::Variant& shape_b = *particles_[pb].shape;
                clam::Vec3d dist = box_->minimum_image(particles_[pb].pos - particles_[pa].pos);
            
                if(dist.length2() < sqr(min_overlap_distance(particles_[pa].size, shape_a, particles_[pb].size, shape_b))){
                    return true;
                }
                else if(dist.length2() < sqr(max_overlap_distance(particles_[pa].size, shape_a, particles_[pb].size, shape_b))){
                    Particle ppa = particles_[pa];
                    Particle ppb = particles_[pb];
                    ppa.pos = clam::Vec3d(0.0);
                    ppb.pos = dist;
                    bool overlap = shape_overlap(ppa, shape_a, ppb, shape_b);
                    if(overlap) return true;
                }
                return false;
            })
        ){
            auto temp_box = box_;
            box_ = old_box;
            delete temp_box;
            if(cll_rebuild){
                auto temp_cll = cll_;
                cll_ = cll_old;
                delete temp_cll;
            }
            for(size_t i = 0; i < n_part_; ++i) particles_[i].pos = old_positions[i];
            return false;
        }

        delete old_box;
        if(cll_rebuild) delete cll_old;

        rcm_drift_ = 0.0; //Just reset the drift for Volume moves.

        return true;
    }
    auto temp_box = box_;
    box_ = old_box;
    delete temp_box;
    return false;
}

//TODO: Maybe make this a lambda that can be passed to Simulation
static inline double cluster_function(double dist, double delta){
    return (dist < delta)? 1.0 - sqr(dist / delta): 0.0;
}

//@note: Using a hash_map to cache distances calculated in the first
//step, improves performance slightly.
bool Simulation::cluster_change_volume(const clam::Vec3d& ds){
    Particle* particles_ = config_.particles_;
    box::Base*& box_     = config_.box_;
    size_t n_part_       = config_.n_part_;

    //TODO: Make this a Simulation variable instead
    static double delta = 0.01;

    clam::Vec3d factor = (ds / box_->bounds()) + 1.0;
    if(!box_->is_hard()){
        double min_cell_size = cll_->cell_min_size();
        clam::Vec3d bounds = box_->bounds();
        for(size_t d = 0; d < 3; ++d){
            if(factor[d] * bounds[d] < min_cell_size) return false;
        }
    }

    size_t n_clusters = 0;
    size_t sp = 0;
    auto stack         = std::unique_ptr<size_t[]>(new size_t[n_part_]);
    auto cluster_idx   = std::unique_ptr<size_t[]>(new size_t[n_part_]{});
    auto cluster_sizes = std::unique_ptr<size_t[]>(new size_t[n_part_ + 1]);
    auto cms           = std::unique_ptr<clam::Vec3d[]>(new clam::Vec3d[n_part_ + 1]);
    size_t max_cluster = 0;

    for(size_t i = 0; i < n_part_; ++i){
        if(!cluster_idx[i]){
            ++n_clusters;
            size_t cluster_size = 0;
            cluster_idx[i] = n_clusters;
            stack[sp++] = i;

            auto xi_av   = clam::Vec3d(0.0);
            auto zeta_av = clam::Vec3d(0.0);
            auto pos_av  = clam::Vec3d(0.0);

            while(sp){
                size_t pid = stack[--sp];

                //Center of mass calculation for hard boxes
                if(box_->is_hard()){
                    pos_av += particles_[pid].pos;
                }
                else
                {//Center of mass calculation for periodic boxes
                    auto theta = 2.0 * M_PI * particles_[pid].pos / box_->bounds();
                    clam::Vec3d xi, zeta;
                    for(int j = 0; j < 3; ++j){
                        xi[j]   = cos(theta[j]);
                        zeta[j] = sin(theta[j]);
                    }
                    xi_av   += xi;
                    zeta_av += zeta;
                }
                ++cluster_size;

                Particle pa = particles_[pid];
                pa.pos = clam::Vec3d(0.0);
                const shape::Variant& shape_a = *particles_[pid].shape;

                for(auto cid: cll_->particle_cell_nbs(pid)){
                    for(auto nb_pid: cll_->cell_content(cid)){
                        if((nb_pid != int(pid)) && !cluster_idx[nb_pid]){
                            auto cc_dist = box_->minimum_image(particles_[nb_pid].pos - particles_[pid].pos);
                            const shape::Variant& shape_b = *particles_[nb_pid].shape;
                            double max_dist2 = sqr(max_overlap_distance(particles_[pid].size, shape_a, particles_[nb_pid].size, shape_b) + delta);
                            if(cc_dist.length2() < max_dist2){
                                Particle pb = particles_[nb_pid];
                                pb.pos = cc_dist;
                                if(shape_overlap(pa, shape_a, pb, shape_b, delta)){
                                    auto dist = shape_distance(pa, shape_a, pb, shape_b);
                                    if(rand01() < cluster_function(dist.length(), delta)){
                                        stack[sp++] = nb_pid;
                                        cluster_idx[nb_pid] = n_clusters;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if(box_->is_hard()){
                cms[n_clusters] = pos_av / cluster_size;
            }
            else{
                for(int i = 0; i < 3; ++i){
                    cms[n_clusters][i] = 0.5 * box_->bounds()[i] * (atan2(-zeta_av[i] / cluster_size, -xi_av[i] / cluster_size)/M_PI + 1.0);
                }
            }

            if(cluster_size > max_cluster) max_cluster = cluster_size;

            cluster_sizes[n_clusters] = cluster_size;
        }
    }

    //TODO: Make this more intelligent
    if(max_cluster > n_part_ / 2){
        delta *= 0.5;
        return false;
    }

    //Copy the old box for backup
    auto old_box = box_->clone();
    //Change the box size to the new volume
    box_->rescale(factor);

    double old_vol = old_box->volume();
    double new_vol = box_->volume();

    double dv = new_vol - old_vol;
    double fraction = new_vol / old_vol;

    auto old_positions = std::unique_ptr<clam::Vec3d[]>(new clam::Vec3d[n_part_]);
    for(size_t i = 0; i < n_part_; ++i){
        old_positions[i] = particles_[i].pos;
        particles_[i].pos = (cluster_sizes[cluster_idx[i]] == 1)?
            particles_[i].pos * factor:
            box_->apply_pbc(old_box->minimum_image(particles_[i].pos - cms[cluster_idx[i]]) + cms[cluster_idx[i]] * factor);
        if(box_->is_hard() && box_->check_overlap(particles_[i], *particles_[i].shape)){
            auto temp_box = box_;
            box_ = old_box;
            delete temp_box;
            for(size_t j = 0; j <= i; ++j) particles_[j].pos = old_positions[j];
            return false;
        }
    }

    //@note: I tried avoiding rebuilding by rescaling and updating, but it
    //is only slightly faster.
    auto cll_new = new CellList();
    cll_new->init(n_part_, box_->bounds(), cll_->cell_min_size());
    for(size_t i = 0; i < n_part_; ++i) cll_new->add(i, particles_[i].pos);

    double log_prob = 0.0;

    //@note: For expansion we could in principle merge the two foreach calls
    //and use the old cell list. For simplicity we keep it like this for now.
    bool rejected = foreach_pair(*cll_new, [this, particles_, box_, &cluster_idx, &log_prob](int pa, int pb) -> bool {
        if(cluster_idx[pa] == cluster_idx[pb]) return false;

        const shape::Variant& shape_a = *particles_[pa].shape;
        const shape::Variant& shape_b = *particles_[pb].shape;
        clam::Vec3d cc_dist = box_->minimum_image(particles_[pb].pos - particles_[pa].pos);

        if(cc_dist.length2() < sqr(min_overlap_distance(particles_[pa].size, shape_a,
                                                        particles_[pb].size, shape_b))
        ){
            return true;
        }
        else if(cc_dist.length2() < sqr(max_overlap_distance(particles_[pa].size, shape_a,
                                                             particles_[pb].size, shape_b) + delta)
        ){
            Particle ppa = particles_[pa];
            Particle ppb = particles_[pb];
            ppa.pos = clam::Vec3d(0.0);
            ppb.pos = cc_dist;
            if(shape_overlap(ppa, shape_a, ppb, shape_b, delta)){
                auto dist = shape_distance(ppa, shape_a, ppb, shape_b);
                if(dist.length2() == 0.0) return true;
                log_prob += log(1.0 - cluster_function(dist.length(), delta));
            }
        }
        return false;
    });

    if(!rejected){
        foreach_pair(*cll_, [this, particles_, &cluster_idx, &log_prob, old_box, &old_positions](int pa, int pb) -> bool {
            if(cluster_idx[pa] == cluster_idx[pb]) return false;

            const shape::Variant& shape_a = *particles_[pa].shape;
            const shape::Variant& shape_b = *particles_[pb].shape;
            clam::Vec3d cc_dist = old_box->minimum_image(old_positions[pb] - old_positions[pa]);

            if(cc_dist.length2() < sqr(max_overlap_distance(particles_[pa].size, shape_a,
                                                            particles_[pb].size, shape_b) + delta)
            ){
                Particle ppa = particles_[pa];
                Particle ppb = particles_[pb];
                ppa.pos = clam::Vec3d(0.0);
                ppb.pos = cc_dist;
                if(shape_overlap(ppa, shape_a, ppb, shape_b, delta)){
                    auto dist = shape_distance(ppa, shape_a, ppb, shape_b);
                    log_prob -= log(1.0 - cluster_function(dist.length(), delta));
                }
            }
            return false;
        });

        if(rand01() <= exp(-pressure_ * dv + n_clusters * log(fraction) + log_prob)){
            delete old_box;
            delete cll_;
            cll_ = cll_new;

            rcm_drift_ = 0.0; //Just reset the drift for Volume moves.

            return true;
        }
    }

    auto temp_box = box_;
    box_ = old_box;
    delete temp_box;
    delete cll_new;
    for(size_t i = 0; i < n_part_; ++i) particles_[i].pos = old_positions[i];

    return false;
}

//void Simulation::sort_particles(void){
//    using ParticleIDPair = std::pair<size_t, Particle>;
//    size_t cids[config_.n_part_];
//    std::map<size_t, std::vector<ParticleIDPair>> particles;
//
//    for(size_t i = 0; i < config_.n_part_; ++i){
//        cids[i] = cll_->cell_index(i);
//        particles[cids[i]].emplace_back(i, config_.particles_[i]);
//    }
//
//    size_t pid = 0;
//    //size_t changed = 0;
//    for(auto vec: particles){
//        for(auto pair: vec.second){
//            if(pid != pair.first){
//                config_.particles_[pid] = pair.second;
//                cll_->update(pid, vec.first);
//                //++changed;
//            }
//            ++pid;
//        }
//    }
//    //printf("--%lu\n", changed);
//}

//void Simulation::sort_particles(void){
//    using ParticleIDPair = std::pair<size_t, Particle>;
//    size_t cids[config_.n_part_];
//    std::vector<ParticleIDPair> particles;
//    particles.reserve(config_.n_part_);
//
//    for(size_t i = 0; i < config_.n_part_; ++i){
//        cids[i] = cll_->cell_index(i);
//        particles.emplace_back(i, config_.particles_[i]);
//    }
//
//    auto itr = particles.begin();
//    for(size_t cid = 0; itr != particles.end(); ++cid){
//        itr = std::partition(itr, particles.end(), [cid, &cids](const ParticleIDPair& pair) -> bool {
//                return cids[pair.first] == cid;
//        });
//    }
//
//    //size_t changed = 0;
//    for(size_t pid = 0; pid < config_.n_part_; ++pid){
//        const auto& pair = particles[pid];
//        if(pid != pair.first){
//            config_.particles_[pid] = pair.second;
//            cll_->update(pid, cids[pair.first]);
//            //++changed;
//        }
//    }
//    //printf("--%lu\n", changed);
//}

//void Simulation::sort_particles(void){
//    using ParticleIDPair = std::pair<size_t, Particle>;
//    size_t cids[config_.n_part_];
//    std::vector<ParticleIDPair> particles;
//    particles.reserve(config_.n_part_);
//
//    for(size_t i = 0; i < config_.n_part_; ++i){
//        cids[i] = cll_->cell_index(i);
//        particles.emplace_back(i, config_.particles_[i]);
//    }
//
//    std::sort(particles.begin(), particles.end(), [&cids](const ParticleIDPair& pair_a, const ParticleIDPair& pair_b) -> bool {
//        return cids[pair_a.first] < cids[pair_b.first];
//    });
//
//    //size_t changed = 0;
//    for(size_t pid = 0; pid < config_.n_part_; ++pid){
//        const auto& pair = particles[pid];
//        if(pid != pair.first){
//            config_.particles_[pid] = pair.second;
//            cll_->update(pid, cids[pair.first]);
//            //++changed;
//        }
//    }
//    //printf("--%lu\n", changed);
//}

size_t Simulation::get_npart(void)const{
    return config_.n_part_;
}

double Simulation::get_pressure(void)const{
    return pressure_;
}

double Simulation::get_volume(void)const{
    return config_.box_->volume();
}

clam::Vec3d Simulation::get_drift(void)const{
    return rcm_drift_;
}

//NOTE: Thread-unsafe
const Configuration& Simulation::get_configuration(void)const{
    return config_;
}

//Setters
//TODO: Implement
void Simulation::reseed(uint32_t seed){
}

void Simulation::set_pressure(double pressure){
    pressure_ = pressure;
}

}

//-- C implememtations

MchSimulation* mch_sim_new(const MchConfiguration* config){
    return static_cast<MchSimulation*>(new MCH::Simulation(*config));
}

void mch_sim_delete(MchSimulation* sim){
    auto cpp_sim = static_cast<MCH::Simulation*>(sim);
    delete cpp_sim;
    sim = nullptr;
}

bool mch_sim_rotate_particle(MchSimulation* sim, size_t r_pid, double* quat){
    return sim->rotate_particle(r_pid, clam::Quatd(quat[0], quat[1], quat[2], quat[3]));
}

bool mch_sim_move_particle(MchSimulation* sim, size_t r_pid, double* dr){
    return sim->move_particle(r_pid, clam::Vec3d(dr[0], dr[1], dr[2]));
}

bool mch_sim_event_chain_move(MchSimulation* sim, size_t r_pid, double length, double* rdir){
    return sim->event_chain_move(r_pid, length, clam::Vec3d(rdir[0], rdir[1], rdir[2]));
}

bool mch_sim_change_volume(MchSimulation* sim, double* ds){
    return sim->change_volume(clam::Vec3d(ds[0], ds[1], ds[2]));
}

bool mch_sim_cluster_change_volume(MchSimulation* sim, double* ds){
    return sim->cluster_change_volume(clam::Vec3d(ds[0], ds[1], ds[2]));
}

//Getters
const MchConfiguration* mch_sim_get_configuration(const MchSimulation* sim){
    return &sim->get_configuration();
}

size_t mch_sim_get_npart(const MchSimulation* sim){
    return sim->get_npart();
}

double mch_sim_get_pressure(const MchSimulation* sim){
    return sim->get_pressure();
}

double mch_sim_get_volume(const MchSimulation* sim){
    return sim->get_volume();
}

//Setters
void mch_sim_set_pressure(MchSimulation* sim, double pressure){
    sim->set_pressure(pressure);
}

