#ifndef MCH_UTILITY_H
#define MCH_UTILITY_H

namespace MCH{
    // Fast and accurate cube-root by Kahan
    static inline double fast_cbrt(double k){
        const unsigned int B1 = 715094163u;
        double t = 0.0;
        unsigned int* pt = (unsigned int*) &t;
        unsigned int* px = (unsigned int*) &k;
        pt[1] = px[1] / 3 + B1;
        double x = t;
        for(int i = 0; i < 2; i++){
            double tri = x*x*x;
            x = x * ((tri + k + k) / (tri + tri + k));
        }
        return x;
    }
    
    static inline float fast_cbrt(float k){
        float f = k;
        unsigned int* p = (unsigned int*) &f;
        *p = *p / 3 + 709921077u;
        return f;
    }

    template<typename T>
    static inline T sqr(T val){
        return val * val;
    }
}

#endif
